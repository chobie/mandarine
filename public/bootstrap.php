<?php
declare(ENCODING="utf8");

ini_set('display_errors',"On");
ini_set('log_errors',"On");
putenv("LANG=ja_JP");
setlocale(LC_ALL, 'ja_JP.UTF-8');
@date_default_timezone_set((@date_default_timezone_get() == "") ? "Asia/Tokyo" : @date_default_timezone_get());
if("neutral" == mb_language()) @mb_language("Japanese");

//for `mb_detect_encoding()` hack: easy multibyte detection.
mb_detect_order("ASCII,JIS,EUC,UTF-8,SJIS");
mb_internal_encoding("UTF-8");
mb_regex_encoding("UTF-8");

define("HERMES_CONTEXT","Development");

if(isset($_SERVER['APP_HOME'])){
    define("APP_HOME",$_SERVER['APP_HOME']);
}else{
    define("APP_HOME","/home/web/mandarine.co");
}
if(!isset($_SERVER['HERMES_ROUTE'])){
    $_SERVER['HERMES_ROUTE'] = "Routes.yaml";
}
if(!isset($_SERVER['HERMES_STATE'])){
    $_SERVER['HERMES_STATE'] = "States.yaml";
}

require APP_HOME . "/lib/FrameWork/Mandarine/Autoloader.php";
$loader = new Mandarine\Autoloader();
$loader->registerPrefixes(array(
    "Mandarine"  =>APP_HOME ."/lib/FrameWork/",
    "Table"      =>APP_HOME ."/lib/Bundle",
    "Record"     =>APP_HOME ."/lib/Bundle",
    "Collection" =>APP_HOME ."/lib/Bundle",
    "PEAR"       =>"/usr/share/php"
    )
)->register();