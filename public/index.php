<?php
declare(encoding="utf8");
require "bootstrap.php";

$env = new Mandarine(APP_HOME ."/etc/environment.yaml");
$env->run();
