<?php
namespace Mandarine;

abstract class Controller
{
    protected $response;
    protected $env;
    public $view_class;

    public function has_view(){
        if(class_exists($this->view_class)){
            return true;
        }else{
            return false;
        }
    }
    
    public function __construct(Handler\Response $response,Dispatcher &$env){
        $this->response = $response;
        $this->env = $env;
        
        if($this->__init__() === false){
            throw new InterupptedException();
        }
    }
    
    public function __init__(){
    }
}
