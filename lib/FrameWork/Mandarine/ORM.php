<?php
namespace Mandarine;

class ORM
{
    const DEFAULT_CHARSET = "utf8";
    protected $config;
    protected $connected;
    public static $instances;
    protected $charset;
    protected $models;
    protected $connection_checked_at;
    protected $connection_check_cycle = 60;

    public $mysqli;
    public $structures;

    protected $hostname;
    protected $username;
    protected $password;
    protected $database;
  
    public static function Begin(){
        $instance = self::getInstance();
        $instance->mysqli->autocommit(false);
        $instance->mysqli->query("START TRANSACTION");
    }
  
    public static function Commit(){
        $instance = self::getInstance();
        $result = $instance->mysqli->commit();
        $instance->mysqli->autocommit(true);
        return $result;
    }
  
    public static function Rollback(){
        $instance = self::getInstance();
        return $instance->mysqli->rollback();
    }

    public function generate(){
        $result = $this->mysqli->query("show tables",MYSQLI_STORE_RESULT);
        $tables = array();

        while($row = $result->fetch_row()){
            $tables[] = $row[0];
        }
        $result->close();

        $gen = array();
        foreach($tables as $table){
            $result = $this->mysqli->query("desc {$table}",MYSQLI_STORE_RESULT);
            $desc = array();

            while($assoc = $result->fetch_assoc()):
                foreach($assoc as $key=>$value){
                    $type = strtolower($key);

                    switch($type){
                        case "field":
                            $field = $value;
                            $desc[$field] = array();
                            break;
                        case "type":
                            $desc[$field]["type"] = $value;
                            break;
                            case "key":
                            if($value == "PRI"){
                                $desc[$field]["primary"] = true;
                            }else{
                                $desc[$field]["primary"] = false;
                            }
                            break;
                        case "extra":
                            if($value == "auto_increment"){
                                $desc[$field]["autoincrement"] = true;
                            }
                            break;
                        case "default":
                            $desc[$field]["default"] = $value;
                            break;
                        }
            }
            endwhile;

            $gen[self::Cameraized($table)]["columns"] = $desc;
            $result->close();
        }

        echo yaml_emit($gen);
    }

    public static function Configure($file){
        //if(!is_file($file . ".cache")){
        if(is_array($file)){
            $config = $file;
        }else{
            $config = yaml_parse_file($file);
        }
        //  file_put_contents($file . ".cache",serialize($config),LOCK_EX);
        //}else{
        //    $config = unserialize(file_get_contents($file . ".cache"));
        //}

        $instance = ORM::getInstance();
        $instance->config = $config;    
        $instance->connect($config["connection"]);

        if(isset($config["tables"])){
            foreach($config["tables"] as $table=>$item){
                $table = self::UnCameraized($table);
                $instance->structures[$table] = $item;
            }
        }
    }

    public function connect($url){
        $parsed = parse_url($url);

        if(isset($parsed["host"])){
            $this->hostname = $parsed["host"];
        }

        if(isset($parsed["user"])){
            $this->username = $parsed["user"];
        }

        if(isset($parsed["pass"])){
            $this->password = $parsed["pass"];
        }

        if(isset($parsed["path"])){
            $this->database = ltrim($parsed["path"],"/");
        }

        $this->structures = array();

        $mysqli = $this->mysqli;
        $mysqli->options(MYSQLI_INIT_COMMAND,"set names " . $this->charset);
        $mysqli->real_connect('p:' . $this->hostname,$this->username,$this->password,$this->database);

        if($error = mysqli_connect_error()){
            throw new RuntimeException($error);
        }
        $this->connected = true;

        $mysqli->set_charset($this->charset);
        $this->charset = $mysqli->character_set_name();
    }
  
    public function has_connect(){
        if($this->connected){
            return true;
        }else{
            return false;
        }
    }
  
    public function __construct(){
        self::$instances[spl_object_hash($this)] = $this;
        $this->charset = self::DEFAULT_CHARSET;
        $mysqli = mysqli_init();
        $this->mysqli = &$mysqli;

        $this->structures = array();
        $this->models = array();
    }
  
    public function loadYaml($file){
        if(!isset($this->structures[$file]) && is_file($file)){
            $config = yaml_parse_file($file . ".yaml");
            return $this->structure[$file] = $config[$file];
        }else if(isset($this->structures[$file])){
            return $this->structures[$file];
        }else{
            $result = $this->mysqli->query("desc {$file}",MYSQLI_STORE_RESULT);
            if($error = $this->mysqli->error){
                throw new Exception($error);
            }

            $desc = array();
            while($assoc = $result->fetch_assoc()):
                foreach($assoc as $key=>$value){
                  $type = strtolower($key);

                  switch($type){
                    case "field":
                        $field = $value;
                        $desc[$field] = array();
                        break;
                    case "type":
                        $desc[$field]["type"] = $value;
                        break;
                    case "key":
                        if($value == "PRI"){
                            $desc[$field]["primary"] = true;
                        }else{
                            $desc[$field]["primary"] = false;
                        }
                        break;
                    case "extra":
                        if($value == "auto_increment"){
                            $desc[$field]["autoincrement"] = true;
                        }
                        break;
                    case "default":
                        $desc[$field]["default"] = $value;
                        break;
                    }
                }
            endwhile;
            $this->structures[$file]["columns"] = $desc;
            return $this->structures[$file];
        }
    }

    public function loadStructure($table_name){
        if(isset($this->structures[self::Cameraized($table_name)])){
            return $this->structures[self::Cameraized($table_name)];
        }else{
            return $this->loadYaml($table_name);
        }
    }
  
    public function getCascadeTable($table){
        return $this->structures[$table]["cascading"]["table"];
    }

    public function hasCascadeTable($table){
        if(isset($this->structures[$table]["cascading"]["table"])){
            return true;
        }else{
            return false;
        }
    }
  
    public static function getModel($table){
        $instance = self::getInstance();
        if(isset($instance->models[$table])){
            return $instance->models[$table];
        }else{
            $table_class = ($instance->hasCascadeTable($table)) ? $instance->getCascadeTable($table) : __CLASS__ . "\Model";
            $class_name = ORM::Cameraized($table);
            if(class_exists($class_name)){
                $table_class = $class_name;
            }

            $m_table = new $table_class($table,spl_object_hash($instance));
            return $instance->models[$table] = $m_table;
        }
    }
  
    public function query($sql){
        $result = $this->mysqli->query($sql);
        $res = array();
        while($item = $result->fetch_assoc()){
            $res[] = $item;
        }
        $result->close();
        return $res;
    }
  
  public static function getInstance($hash = null){
        if(!empty($hash)){
            if(isset(self::$instances[$hash])){
                $tmp = self::$instances[$hash];
                if(isset($tmp->mysqli)){
                    if((time() - $tmp->connection_checked_at) > $tmp->connection_check_cycle){
                        if(!$tmp->mysqli->ping()){
                            $tmp->connection_checked_at = time();
                            $tmp->connect($tmp->config["connection"]);
                        }
                    }
                }
                return self::$instances[$hash];
            }else{
                if((bool)self::$instances){
                    foreach(self::$instances as $instance){
                        return $instance;
                    }
                }else{
                    return false;
                }
            }
        }else{
            if((bool)self::$instances){
                foreach(self::$instances as $instance){
                    $tmp = $instance;
                    if(isset($tmp->mysqli)){
                        if((time() - $tmp->connection_checked_at) > $tmp->connection_check_cycle){
                            if(!$tmp->mysqli->ping()){
                                $tmp->connection_checked_at = time();
                                $tmp->connect($tmp->config["connection"]);
                            }
                        }
                    }
                    return $instance;
                }
            }else{
                $instance = new ORM();
                $hash = spl_object_hash($instance);
                self::$instances[$hash] = $instance;
                return $instance;
            }
        }
    }

    public static function Cameraized($str){
        return join("",array_map("ucwords",preg_split("/(-|_)/",$str)));
    }

    public static function UnCameraized($str){
        return strtolower(preg_replace_callback("'(?<!^)([A-Z][a-z]*)'",array("ORM",'_UnCameraized'),$str));
    }

    public static function _UnCameraized($matches){
        array_shift($matches);
        $tmp  = "";
        foreach($matches as $match){
            $tmp .= "_" . $match;
        }
        return $tmp;
    }
}
