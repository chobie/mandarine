<?php
namespace Mandarine;

class Dispatcher{
	protected $logger_class = 'Mandarine\Profiler';
	public static $logger;
	protected $routes = array();
  protected $context;
  public $charset = "utf-8";
  public $vars = array();
  
  public $package;
	public $controller;


	public function getLogger(){
		return self::$logger;
	}
	public function log($err_code,$string){
		self::$logger->logging($err_code,$string);
	}

  public function getContext(){
    return $context;
  }

	public function __construct($context){
    $this->context = $context;
      if(is_file(APP_HOME . "/var/cache/" . $context . "/Config.obj")){
        $conf = unserialize(file_get_contents(APP_HOME . "/var/cache/" . $context . "/Config.obj"));
      }else{
        $conf = yaml_parse_file(APP_HOME . "/etc/" . $context . "/Config.yaml");
  	    file_put_contents(APP_HOME . "/var/cache/" . $this->context . "/Config.obj", serialize($conf),LOCK_EX);
      }

    if(!isset(self::$logger)){
	    $logger_class = $this->logger_class;
	    self::$logger = new $logger_class($context);
    }
	}

	public function getRoutes(){
		return $this->routes;
	}

	public function add(Router $route){
		$this->routes[] = $route;
	}
	
	public function autoload($classname){
		if(strpos($classname,$this->package)!==false){
			$classname = str_replace('\\','/',$classname);
			if(is_file(APP_HOME . "/lib/" . $classname . ".php")){
				require_once APP_HOME . "/lib/" . $classname .".php";
				return true;
			}
		}
		return false;
	}
	
	public function invoke(Router $router,Handler\Request $request, Handler\Response $response){
		$this->package = $router->getPackage();
		$this->controller = $router->getController();

    spl_autoload_register(array($this, 'autoload'));
		$this->log(Profiler::DEBUG, "preparing invoke`" . sprintf("\\Application\\%s\\%s::%s()",$router->getPackage(),$router->getController(),$router->getAction()) . "`.");

		$response->format = (isset($router->defaults->format)) ? $router->defaults->format : "html";
/*
		$states = yaml_parse_file(APP_HOME . "/etc/" . $this->context ."/{$_SERVER['HERMES_STATE']}");
		if((bool)$states){
			foreach($states as $key => $obj){
				if($key == "Filters"){
					$filters = new \Mandarine\FilterCollection();
					foreach($obj as $item){
						$filter = new Hermes_Filter($item);
						$filters->add($filter);
					}
				}
			}
		}
*/

		try{
			$conf = (isset($this->conf)) ? $this->conf : array();
      $vars = get_object_vars($request);
      $vars["conf"] = $conf;
      $this->vars = $vars;


			$this->log(Profiler::DEBUG, "invoking `" . sprintf("\\Application\\%s\\%s::%s()",$router->getPackage(),$router->getController(),$router->getAction()) . "`.");
      ob_start();
      $controller_class = "\\Application\\" .$router->getPackage() . "\\Controller\\" . $router->getController();
			$controller = new $controller_class($response,$this);
			$action = $router->getAction() . "Action";

			if(!method_exists($controller,$action)){
				throw new Exception\ActionNotFound("Controller method `{$action}` was not found");
			}

			$bResult = $controller->$action($request);
      $response->content = ob_get_clean();

			$this->log(Profiler::DEBUG, "invoking `" . sprintf("\\Application\\%s\\%s::%s()",$router->getPackage(),$router->getController(),$router->getAction()) . "` finished.");

      $vars = array_merge($this->vars,$vars);

			if($controller->has_view() && $router->hasTemplate() && $bResult !== false){
	      $view_class = $controller->view_class;
	      $view = new $view_class();
	      $view->loadTemplate($router->getTemplate());
	      $response->content .= $view->render($vars);
				$this->log(Profiler::DEBUG, "rendering `" . sprintf("%s",$router->getTemplate()) . "` finished.");
			}


      if(isset($filters) && $output){
				foreach($filters->getFilters() as $filter){
					$this->log(Profiler::DEBUG, sprintf("post filter `%s::%s()` invoked",$filter->class,$filter->method));
					$filter->invoke($response);
				}
			}

      echo $response->content;
			$this->log(Profiler::DEBUG, "sending contents to output buffer.");
		}catch(InterupptedException $e){
			// something catch
		}

	}
	
  public function compile(){
    foreach($this->getRoutes() as $router){
      echo "# " . $router->getLocation() . PHP_EOL;
      echo '<?php' . PHP_EOL;
      echo "\$router = '" . serialize($router) . "';" . PHP_EOL;
      echo "\$Dispatcher->invoke(\$router,\$response);" . PHP_EOL;
      echo PHP_EOL;
    }
  }

	public function it(Handler\Request &$handle,Handler\Response &$response){
    $logger_class = $this->logger_class;
    self::$logger = new $logger_class($this->context);

		$this->log(Profiler::DEBUG, "starting routing iteration. requested url:" . $handle->getLocation() );

    try{
  		foreach($this->getRoutes() as $router){
  			if($router->match($handle)){
  				$this->log(Profiler::DEBUG, "matching `" . $router->getLocation() . "`.");

  				if($router->hasSubroutes()){
  					$target =  APP_HOME . "/lib/Application/" . $router->getPackage() . "/etc/". $_SERVER['HERMES_ROUTE'];

  					if(is_file($target)){
  						$Dispatcher = new self($this->context);
							$routes = yaml_parse_file($target);
							foreach($routes as $conf){
								$Dispatcher->add(Hermes_Router::Load($conf,$router));
							}

		  				$this->log(Profiler::DEBUG, "raise subroutes from `" . $router->getLocation() . "`.");
  						return $Dispatcher->it($handle,$response);
  					}
  				}
  				$this->invoke($router,$handle,$response);
  				break;
  			}
  		}
    }catch(ActionNotFoundException $e){
      echo "404 Not Found";
    }
		
		if($response->has_content()){
			$response->content;
		}
	}
}

