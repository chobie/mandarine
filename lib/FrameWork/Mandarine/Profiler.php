<?php
namespace Mandarine;

class Profiler
{
    const EMERG = 0x01;
    const ALERT = 0x02;
    const CRITICAL = 0x03;
    const ERROR = 0x04;
    const WARN = 0x05;
    const NOTICE = 0x06;
    const INFO = 0x07;
    const DEBUG = 0x08;

    protected $start;
    protected $finished;
    protected $log = array();
    
    public function show(){
        $code = array(1=>"EMERG",2=>"ALERT",3=>"CRITICAL",4=>"ERROR",5=>"WARN",6=>"NOTICE",7=>"INFO",8=>"DEBUG");
        $process_time = str_pad(round($this->finished-$this->start,7),9,"0",STR_PAD_RIGHT);
        echo "<center><table border=1 style='font-size:8px;margin:1em;width:80%;text-align:center;background:#FFF;color:#000;'>";

        foreach($this->log as $obj){
            if(isset($last)){
                //$time = $obj[2]-$last[2];
                //$time = $obj[2] - $last[2];
                $time = str_pad(round($obj[2] - $this->start,6),9,"0",STR_PAD_RIGHT);
                
            }else{
                $time = 0;
            }
            echo "<tr><td>{$code[$obj[0]]}</td><td>{$obj[1]}</td><td>{$time}</td></tr>";
            $last = $obj;
        }
        echo "<tr><th>Processing Time</th><td></td><td>{$process_time}</td></tr>";
        echo "</table></center>";
    }
    
    public function timestamp(){
        return microtime(true);
    }
    
    public function logging($err_code = self::DEBUG,$text){
        $this->log[] = array($err_code,$text,$this->timestamp());
    }

    public function __construct($context){
        $this->start = $this->timestamp();
        $this->logging(self::DEBUG,"profiler enabled");
    }
    
    public function end(){
        $this->finished = $this->timestamp();
        $this->logging(self::DEBUG,"profiler finished");
    }
}
