<?php
declare(encoding="utf8");
namespace Mandarine\View;

class Template
{
    public $template;
    
    public function __construct(){
    }
    
    public function loadTemplate($filename){
        $orange = new \Mandarine\Template(array(
            'cache' => APP_HOME . "/var/cache/templates/public",
            'templates' => APP_HOME . "/var/templates/public"
        ));

        $orange->loadTemplate($filename);
        $this->template = $orange;
    }
    
    public function render(array $context){
        return $this->template->render($context);
    }

    public function display(array $context){
        $this->template->display($context);
    }
}