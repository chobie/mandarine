<?php
declare(encoding="utf8");
namespace Mandarine\View;

class PHP{
    public $template;
    
    public function loadTemplate($filename){
        $this->template = APP_HOME . "/var/templates/public/" . ltrim($filename,"/");
    }
    
    public function render(array $context){
        ob_start();
        $this->display($context);
        return ob_get_clean();
    }

    public function display(array $context){
        extract($context);
        include $this->template;
    }
}