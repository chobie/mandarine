<?php
namespace Mandarine\Template;

abstract class Base{
  protected $env;

  public function __construct(&$env){
    $this->env = $env;
  }

  public function evaluate($lines,$context){
    extract($context);
    $stmt = $this->compile($lines);
    eval('?><?php ' . $stmt);
  }
  
  public function compile($lines){
    return $this->env->compile($lines);
  }
}