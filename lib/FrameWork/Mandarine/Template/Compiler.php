<?php
namespace Mandarine\Template;

class Compiler{
  const SEEK_SET = 1;
  const SEEK_CUR = 2;
  const SEEK_END = 3;

  protected $env;
	public $indent = 1;
  public $scope_target = array("display");
	public $scope = array();
  public $foreachelse = array();
  public $foreach_vars =  array();
  public $foreach_nest = 0;

  public function clear(){
  	$indent = 1;
    $scope_target = array("display");
    $this->scope = array("display"=>null);
    $foreachelse = array();
    $foreach_vars =  array();
    $foreach_nest = 0;
  }

  public function __construct(&$env){
    $this->env = $env;
    $this->scope = array("display"=>null);
  }

  public function parse_variable($result,$bModifier = true){
  	$o=0;
  	while(preg_match('/\$[a-zA-Z_\x7f-\xff][]"\'a-zA-Z0-9_\x7f-\xff.$[]*/',substr($result,$o),$m,PREG_OFFSET_CAPTURE)){
  		$r = "";
  		if(strpos($m[0][0],".")!==false){
  			foreach(explode(".",$m[0][0]) as $i):
  				if(strpos($i,"[")!==false){
  					$r .= preg_replace("/([a-zA-Z_\x7f-\xff][a-zA-Z_\x7f-\xff]*)(.*)/","['$1']$2",$i);
  				}else{
            if(strpos($i,"]")!==false){
      				$r .= (empty($r)) ? $i : "['". trim($i,"[]")  . "']]" ;
            }else{
      				$r .= (empty($r)) ? $i : "['". $i . "']" ;
            }
  				}
  			endforeach;
  		}

  		$r = (empty($r)) ? $m[0][0] : $r;
  		$result = substr_replace($result ,$r,$o+$m[0][1],strlen($m[0][0]));
  		$o = $o+$m[0][1]+strlen($r);
  	}
  	
    if($bModifier && strpos($result,"|")){
      $expression = explode("|",$result);
      //krsort($expression);

      $variable = trim(array_shift($expression));
      $modifier = "";
      foreach($expression as $item){
      	$flag = false;
        try{
          $item = trim($item);

          $func = array();
          if(strpos($item,":")!==false){
            $func = explode(":",$item);
            $item = array_shift($func);
            foreach($func as $offset=>$params){
            	if($params[0] == "'" || $params[0] == "\""){
	              $func[$offset] = "'" . trim($params,"\"\' \t\n\r\0\x0B") . "'";
            	}else{
	              $func[$offset] = trim($params,"\"\' \t\n\r\0\x0B");
              }
            }
          }

          $r = new ReflectionFunction($item);
          $params = $r->getParameters();

          $para = array();
          $flag = false;
          foreach($params as $offset=>$param):
          	//var_dump($param->isDefaultValueAvailable());

            if($param->getName() == "str"){
              $para[] = $variable;
              $flag = true;
            }else{
            	if(!$param->isOptional() && !(bool)$func){
            		throw new RuntimeException("function `" . $item . "` must need \$" . $param->getName() . " parameter(#$offset}).");
            	}
              if((bool)$func){
                $para[] = array_shift($func);
              }
            }
          endforeach;

          if(!$flag) throw new RuntimeException("modifier function must have \$str parameter");

        }catch(Exception $e){
          throw $e;
        }

        if(empty($modifier)){
          $modifier = $item . "(". join(", ",$para) . ")";
        }else{
          $modifier = $item . "(" . $modifier . ")";
        }
      }
      $result = $modifier;
    }
    
  	return $result;
  }
  
  public function process_super(){
		$indent = str_repeat("  ",$this->indent);
    $method = $this->scope_target[count($this->scope_target)-1];
    return $indent . "parent::block_{$method}(\$context);\r\n";
  }

  public function process_link_to($statement){
  	$params = $this->parse_statement($statement);
		$indent = str_repeat("  ",$this->indent);
    return $indent . "\$this->link_to(" .$this->var_export($params) . ");\r\n";
  	
  }
  
  public function process_for($statement){
    if(strpos($statement,"in") !== false){
      $this->for_type = 1;
      $e = explode(" ",trim($statement),3);
      return "{$indent}if((bool)$e[2]):\r\nforeach($e[2] as $e[0]):\r\n";
    }else{
      $this->for_type = 2;
      return "for($statement):\r\n";
    }
  }
  
  public function process_endfor(){
		$indent = str_repeat("  ",$this->indent);

    switch($this->for_type){
      case "1":
        $replace = $indent . $indent . "endforeach;endif;\r\n";
        break;
      case "2":
        $replace = $indent . $indent . ";endfor;\r\n";
        break;
    }
    $this->for_type = 0;
    return $replace;
  }
  
  public function process_foreach($m){
    $this->foreach_vars[] = trim(substr($m,0,strpos($m,"as")));
    $r = explode(" ",trim($m),3);
    $r[0] = $this->parse_variable($r[0]);
    $key = explode(" ",$r[0]);
    $this->foreach_nest++;
		$indent = str_repeat("  ",$this->indent);

		$this->indent+=3;
    return "{$indent}if((bool)$key[0]):\r\n{$indent}{$indent}foreach(" . join(" ",$r) . "):\r\n";
  }
  
  public function process_foreachelse($m){
    $this->foreachelse[$this->foreach_nest] = $m;
    return "endforeach;\r\nelse:\r\n";
  }
  
  public function process_endforeach($last){
    $pop = array_pop($this->foreach_vars);
		$indent = str_repeat("  ",max($this->indent-3,0));

    if(isset($this->foreachelse[$this->foreach_nest])){
      unset($this->foreachelse[$this->foreach_nest]);
    }else{
      $result = $indent . $indent ."endforeach;\r\n";
    }
    $result .= $indent . "endif;\r\n";

		$this->indent-=2;
    $this->foreach_nest--;
    return $result;
  }
  public function process_continue($statement){
		$indent = str_repeat("  ",$this->indent);
    return $indent . "continue;\r\n";
  }

  public function process_import($statement,$context){
		$indent = str_repeat("  ",$this->indent);
    return $indent . "\$this->env->Import(" . $statement . ",\$context);\r\n";
  }
  
  public function process_extends($statement){
    $this->env->extend = trim($statement,"\"\'\r\n ");
    $this->extends = trim($statement,"\"\'\r\n ");
  }

  public function var_export($params){
    if(is_array($params)){
      $result = "array(";
      $__ = array();
      foreach($params as $key => $value){
        $tmp = "\"{$key}\"=>";
        if($value[0] == "\$"){
          $tmp .= $value;
        }elseif(preg_match("/\(.+?\)/",$value)){
          $tmp .= $value;
        }else{
          $tmp .= "\"$value\"";
        }
        $__[] = $tmp;
      }
      $result .= join(",",$__) . ")";
    }
    return $result;
  }

  public function parse_statement($statement){
    $result = array();
  	while(preg_match("|(?P<key>[a-zA-Z_][a-zA-Z0-9_-]+)=(?P<value>[^&\s]+)|",$statement,$match,PREG_OFFSET_CAPTURE)){
  		$key = $match['key'][0];
  		$value = $this->parse_variable(trim($match['value'][0],"\"' \t\r\n"),true);
      $result[$key] = $value;
  		$statement = substr($statement,$match[0][1]+strlen($match['0']['0']));
    }
    return $result;
  }  

  public function process_using($statement){
  	list($class,$to) = explode(" as ",$statement);
		$indent = str_repeat("  ",$this->indent);
  	return $indent . "{$to} = new {$class}();\r\n";
  }
  
  public function process_eval($statement){
    return "\$this->evaluate(" . $this->parse_variable($statement)  . ",\$context);\r\n";
  }
  
  public function process_block($statement){
    $this->scope_target[] = trim($statement);
		$indent = str_repeat("  ",max($this->indent,0));
    return $indent . "\$this->block_$statement(\$context);\r\n";
  }
  
  public function process_endblock($statement){
    array_pop($this->scope_target);
    if(!(bool)$this->scope_target){
      $this->scope_target = array("display");
    }
    //$this->scope_target = "display";
		$indent = str_repeat("  ",max($this->indent,0));
    //return $indent . "\$this->endblock();\r\n";
  }
  
  public function process_if($statement){
		$indent = str_repeat("  ",max($this->indent,0));

  	$this->indent++;
    return $indent . "if(" . $this->parse_variable(trim($statement,"%\r\n\t "),false) ."):\r\n";
  }
  
  public function process_elseif($statement){
    return "elseif(". $this->parse_variable(trim($statement,"%\r\n\t "),false) ."):\r\n";
  }
  
  public function process_else($statement){
		$indent = str_repeat("  ",max($this->indent-1,0));
    return $indent . "else:\r\n";
  }
  
  public function process_endif($statement){
		$indent = str_repeat("  ",max($this->indent-1,0));
  	$this->indent--;
    return $indent . "endif;\r\n";
  }
  
  public function read($length = 1){
    $result = substr($this->string,$this->current(),$length);
    $this->seek($length,self::SEEK_CUR);
    return $result;
  }
  
  public function vread($length = 1){
    return substr($this->string,$this->current(),$length);
  }

  public function seek($location,$whence = self::SEEK_SET){
    $result = -1;
    switch($whence){
      case self::SEEK_SET:
        $this->cursor = $location;
        $result = 1;
        break;
      case self::SEEK_CUR;
        $this->cursor = $this->cursor+$location;
        $result = 1;
        break;
      case self::SEEK_END;
        $this->cursor = strlen($this->string)+$location;
        $result = 1;
        break;
    }
    if($this->cursor < 0){
    	$this->cursor = 0;
    }
    return $result;
  }
  
  public function current(){
    return $this->cursor;
  }
  
  public function replace($replacement,$start,$end,$scope){
    $r_length = strlen($replacement);
    $s_length = strlen(substr($this->string,$start,$end));
    if(!isset($this->scope[$this->scope_target[count($this->scope_target)-1]])){$this->scope[$this->scope_target[count($this->scope_target)-1]]="";}
    $this->scope[$scope] .= $replacement;
    $this->string = substr_replace($this->string,$replacement,$start,$end);
    
    if($r_length > $s_length){
      $this->length = strlen($this->string);
      $this->seek($r_length-$s_length,self::SEEK_CUR);
    }else if($r_length < $s_length){
      $this->length = strlen($this->string);
      $this->seek(($s_length-$r_length)*-1, self::SEEK_CUR);
    }
  }
  
  public function quote($string){
    $length = strlen($string);
    $result = "";
    for($i=0;$i<$length;$i++){
      if($string[$i] == "\"" || $string[$i] == "\$" || $string[$i] == "\\"){
        $result .= "\\";
      }
      $result .= $string[$i];
    }
    return $result;
  }
  
  public function compile($string){
    $this->string = $string;
    $current_scope = $this->scope_target[count($this->scope_target)-1];
    $this->length = strlen($string);
    $token = $statement = $last = "";
    $this->cursor = $mode = $tag_start_offset = $tag_end_offset = $token_length = 0;

    while($this->cursor < $this->length){
      $current_scope = $this->scope_target[count($this->scope_target)-1];

      $current = $this->string[$this->cursor];
      if($this->cursor+1<$this->length){
        $next = $this->string[$this->cursor+1];
      }

      if($mode == 0 && ($current == "{" && $next == "{")){
//      	printf("[%d,%d]\r\n",$tag_end_offset,$this->cursor);
//      	var_dump(substr($this->string,$tag_end_offset,$this->cursor-$tag_end_offset));
				if(!empty($token)){
					$indent = str_repeat("  ",max($this->indent,0));
          //$replacement = $indent . "echo \"" .preg_replace("/\r?\n/","\\r\\n",ltrim($this->quote($token))) ."\";\r\n";

	      	$length = mb_strlen($token,"utf8");
	      	$i = 0;
	      	while(!empty($token)){
	      		$p[] = $indent . "echo \"" . preg_replace("/\r?\n/","\\r\\n",($this->quote(mb_substr($token,0,76,'utf8')))) ."\";";
	      		$token = mb_substr($token,76,$length,"utf8");
	      		$i++;
	      	}
	      	$replacement = join("\r\n",$p);
	        $this->replace($replacement,$tag_end_offset,$this->cursor-$tag_end_offset,$current_scope);
        }
//        var_dump(substr($this->string,$this->cursor));
        $tag_start_offset = $this->cursor;
        $mode = 1;
        $capture = $token;
        $token = "";
        $token_length = 0;
        $this->cursor += 2;
        continue;
      }else if($mode == 1 && ($current == "}" && $next == "}")){
        $mode = 0;
        $statement = $token;
        $token = "";
        $token_length = 0;

        $tag_end_offset = $this->cursor+2;
        $l = strlen($statement)-1;
        if($statement[0] == "*" && $statement[$l] == "*"){
          // remove comment
          $replacement = "";
        }else{
          $stat = explode(" ",trim($statement,"%\r\n\t "));
          $key = array_shift($stat);
          $moe = trim(join(" ",$stat));

          $method = "process_" . $key;
          if(method_exists($this,$method)){
            $replacement = $this->$method($moe,$capture);
          }else{
						$indent = str_repeat("  ",max($this->indent,0));
          	if(strpos($statement,"->") !== false){
          		
          		list($key,$params) = explode(" ",trim($statement,"%\r\n\t "),2);
          		if(strpos($key,"if")!== false){
	          		$replacement = $indent . 'echo ' . $key . '(' . $this->parse_variable($params) . ");\r\n";
          		}else{
	          		$replacement = $indent . 'echo ' . $key . '(' . $this->var_export($this->parse_statement($params)) . ");\r\n";
          		}
          	}else{
	            $replacement = $indent . 'echo ' . $this->parse_variable(trim($statement,"%\r\n\t "),true). ";\r\n";
          	}
          }
        }
        $this->replace($replacement,$tag_start_offset,$tag_end_offset-$tag_start_offset,$current_scope);
        $tag_end_offset = $this->cursor+2;
        //$this->cursor++;
        continue;
      }else if($mode == 1 && $current == "*" && $token_length == 0){
        $mode = 3;
	      $this->cursor++;
      }else if($mode == 3 && ($last == "*" && $current == "}" && $next == "}")){
        $mode = 0;
        $tag_end_offset = $this->cursor+2;
        $this->replace("",$tag_start_offset,$tag_end_offset-$tag_start_offset,$current_scope);

        $token = "";
        $token_length = 0;
        $tag_end_offset = $this->cursor+2;
	      $this->cursor++;
        continue;
      }
      
      $token .= $current;
      $token_length++;
      $last = $current;
      $this->cursor++;
    }
    if(strlen($token)){
			$indent = str_repeat("  ",max($this->indent,0));
      
      if(!empty($token)){
      	$length = mb_strlen($token,"utf8");
      	$i = 0;
      	while(!empty($token)){
      		$p[] = $indent . "echo \"" . preg_replace("/\r?\n/","\\r\\n",($this->quote(mb_substr($token,0,76,'utf8')))) ."\";";
      		$token = mb_substr($token,76,$length,"utf8");
      		$i++;
      	}
      	$replacement = join("\r\n",$p);
        //$replacement = $indent . "echo \"" . preg_replace("/\r?\n/","\\r\\n",($this->quote($token))) ."\";\r\n";
      	$this->replace($replacement,$tag_end_offset,$this->cursor-$tag_end_offset,$current_scope);
      }
    }
    $result = $this->string;
    $this->string = "";
    $this->length = 0;
    $this->cursor = 0;
    return $result;
  }
  
  public function make_class($filename, $classname,$extends,$extends_file){
    $result  = "<?php\r\n";
    if($extends){
    	$result .= "require_once \"{$extends}.php\";\r\n\r\n";
    }

    $result .= "/* {$filename} */\r\n";
    $result .= "class {$classname}";
    if($extends){
      $result .= " extends {$extends}";
    }else{
      $result .= " extends Mandarine\\Template\\Base";
    }
    $result .= "{\r\n";
    $result .= "  public static \$origin_file = \"{$filename}\";\r\n";
    if($extends){
			$result .= "  public static \$extends_file = \"{$extends_file}\";\r\n\r\n";
    }else{
      $result .= "  public static \$extends_file = \"\";\r\n\r\n";
    }
    $result .= "  public static function getOrigin(){\r\n    return self::\$origin_file;\r\n  }\r\n\r\n";
    $result .= "  public static function getExtends(){\r\n    return self::\$extends_file;\r\n  }\r\n\r\n";

    foreach($this->scope as $key=>$value){
      if($key == "display"){
        if(empty($extends)){
          $result .= "  public function display(Array \$context){\r\n";
          $result .= "    extract(\$context);\r\n";
          foreach(explode("\r\n",$value) as $line){
            $result .= "    " . ltrim($line) . "\r\n";
          }
          $result .= "  }\r\n";
          $result .= "\r\n";
        }
      }else{
        $result .= "  public function block_{$key}(array \$context){\r\n";
        $result .= "    extract(\$context);\r\n";
        foreach(explode("\r\n",$value) as $line){
          $result .= "    " . ltrim($line) . "\r\n";
        }
        $result .= "  }\r\n";
        $result .= "\r\n";
      }
    }
    $result .= "}\r\n";
    return $result;
  }
}