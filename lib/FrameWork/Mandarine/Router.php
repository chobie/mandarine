<?php
namespace Mandarine;

class Router
{
    protected $name;
    protected $location;
    protected $package;
    protected $action;
    protected $controller;
    protected $template;
    protected $subroutes;
    protected $prematch;

    public function hasSubroutes(){
        if(!empty($this->subroutes)){
            return true;
        }else{
            return false;
        }
    }
    
    public function getSubRoutes(){
        return $this->subroutes;
    }

    public function getPackage(){
        return $this->package;
    }

    public function getAction(){
        return $this->action;
    }

    public function getController(){
        return $this->controller;
    }

    public function getTemplate(){
        return $this->template;
    }
  
    public function hasTemplate(){
        return (!empty($this->template)) ? true : false;
    }
  
    public static function checkMark($location){
        if(($offset = strpos($location,"("))!== false){
            if(!isset($result) || $result > $offset){
                $result = $offset;
            }
        }
        if(($offset = strpos($location,":"))!== false){
            if(!isset($result) || $result > $offset){
                $result = $offset;
            }
        } 
        if(($offset = strpos($location,"@"))!== false){
            if(!isset($result) || $result > $offset){
                $result = $offset;
            }
        }
        if(!isset($result)){
            return false;
        }else{
            return $result;
        }
    }

    public static function Load($conf,Router $router = null){
        if(isset($router)){
            $router = clone $router;
        }else{
            $router = new self();
        }
        $router->name = $conf["name"];
        $router->location= $conf["location"];
        if(($offset = self::checkMark($conf["location"])) !== false){
            $router->prematch = substr($conf["location"],0,$offset);
        }else{
            $router->prematch = $conf["location"];
        }

        if(isset($conf["defaults"]["@package"]))
        $router->package = $conf["defaults"]['@package'];

        if(isset($conf["defaults"]["@action"]))
        $router->action = $conf["defaults"]['@action'];

        if(isset($conf["defaults"]["@controller"]))
        $router->controller = $conf["defaults"]['@controller'];

        if(isset($conf["defaults"]['@template'])){
        $router->template =  ltrim($conf["defaults"]['@template'],"/");
    }
    if(isset($conf['subroutes'])){
            $router->subroutes = $conf["subroutes"];
        }
        return $router;
    }
  
    public function getLocation(){
        return $this->location;
    }

    public function __construct(){
    }
    
    public function match(Handler\Request $handler){
        $location = $handler->getLocation();
        $props = array();
        $params = array();

        if(strpos($location,$this->prematch) !== 0){
            return false;
        }

        if(preg_match_all("|@([a-zA-Z0-9_-]+)|",$this->location,$result)){
            foreach($result[1] as $value){
                $props[] = $value;
            }
        }
        $match = preg_replace("|@([a-zA-Z0-9_-]+)|","(?P<$1>([^/.]+))",$this->location);

        if(preg_match_all("|:([a-zA-Z0-9_-]+)|",$match,$result)){
            foreach($result[1] as $value){
                $params[] = $value;
            }
        }

        $match = preg_replace("|:([a-zA-Z0-9_-]+)|","(?P<$1>([^/.]+))",$match);
        if(!isset($this->subroutes)){
            $match .= '$';
        }

        if(preg_match("`^{$match}`",$location,$result)){
            $handler->expired(strlen($result[0]));

            foreach($props as $key){
                if(isset($result[$key])){
                    $this->$key = $result[$key];
                }
            }

            foreach($params as $key){
                if(isset($result[$key])){
                    $handler->$key = $result[$key];
                }
            }
            
          if(preg_match_all("|@([^/.]+)|",$this->template,$match)){
              foreach($match[1] as $value){
          $this->template = str_replace("@" . $value ,$this->$value,$this->template);
              }
          }
          if(preg_match_all("|:([^/.]+)|",$this->template,$match)){
              foreach($match[1] as $value){
          $this->template = str_replace(":" . $value ,$result[$value],$this->template);
              }
          }
            return true;
        }else{
            return false;
        }
    }
}
