<?php
namespace Mandarine;

class FilterCollection{
    public $filters;
    
    public function __construct(){
        $this->filters = array();
    }
    
    public function getFilters(){
        return $this->filters;
    }
    
    public function add(Filter $filter){
        $this->filters[] = $filter;
    }
}
