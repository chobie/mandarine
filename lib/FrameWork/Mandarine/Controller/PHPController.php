<?php
namespace Mandarine\Controller;

class PHPController extends \Mandarine\Controller
{
    public $view_class = "\Mandarine\View\PHP";

    public function modelFactory($table){
        $class = str_replace("\\Controller\\","\\Model\\",get_class($this));
        $class = substr($class,0,(strrpos($class,'\\')+1)) . \Mandarine\ORM::Cameraized($table) . "Model";
        $instance = \Mandarine\ORM::getInstance();

        $model = new $class($table,spl_object_hash($instance));
        return $model;
    }

    public function assign($key,$value){
        if(method_exists($value,"toArray")){
            $value = $value->toArray();
        }
        $this->env->vars[$key] = $this->sanitize($value);
    }

    public function sanitize($object){
        $result = array();
        if(is_array($object) || is_object($object)){
            foreach($object as $key=>$item){
                if(is_array($item) || is_object($item)){
                    $result[$key] = $this->sanitize($item);
                }else{
                    $result[$key] = htmlentities($item,ENT_QUOTES,$this->env->charset);
                }
            }
        }else{
            $result = htmlentities($object,ENT_QUOTES,$this->env->charset);
        }
        return $result;
    }

    public function assign_unsafe($key,$value){
        $this->env->vars[$key] = $value;
    }
}