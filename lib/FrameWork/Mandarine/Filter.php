<?php
namespace Mandarine;

class Filter{
	public $class;
	public $method;
	public $states = true;
	public $type = "post";

	public function __construct($item = array()){
		$this->class = $item["class"];
		$this->method = $item["method"];
		$this->states = ($item["states"] == "enable") ? true : false;
		$this->type = ($item["type"] == "post") ? "post" : "pre";
	}

	public function invoke(Handler\Response &$response){
		if($this->states){
			$class = $this->class;
			$method = $this->method;
			$body = $class::$method($response);
		}
	}
}
