<?php
namespace Mandrine\Helper;

class Math{
  public function percent($params){
    extract($params);

  	if($num && $den){
  		$number = $num/$den*100;
  	} else {
  		$number = 0;
  	}
  	return(number_format($number,2, '.', ''));
  }
}
