<?php
namespace Mandarine\Helper;

class Form{
  public function recive($params){
    extract($params);
    if(is_array($array)){
      foreach($array as $key=>$value){
        if(is_array($value)){
          $result .= $this->recive($value,$expected);
        }else{
          $result .= $this->hidden(array("name"=>$key,"value"=>$value));
        }
      }
    }
    return $result;
  }
  public function validate($params){
    extract($params);
    $result = "";
    
    if((bool)$errors){
      foreach($errors as $error=>$message){
        if(isset($$error)){
          $message = $$error;
        }
        $result .= "<div class=\"errors\">" . $message . "</div>\r\n";
      }
    }
    return $result;
  }
	public function checkbox($params){
		extract($params);
		$attributes = array();
		$attributes["name"] = $name;
		$attributes["value"] = (isset($value)) ? $value : $_REQUEST[$attributes["name"]];
		$attributes["type"] = (isset($type)) ? $type : "checkbox";

		if(isset($style)){
			$attributes["style"] = $style;
		}
		if(isset($class)){
			$attributes["class"] = $class;
		}
		if(isset($id)){
			$attributes["id"] = $id;
		}
		$result = array();
		foreach($attributes as $key=>$value){
			$result[] = "{$key}=\"{$value}\"";
		}
		
		return "<label><input " . join(" ",$result) ." />{$label}</label>";
	}

	public function password($params){
		extract($params);
		$attributes = array();
		$attributes["name"] = $name;
		$attributes["value"] = (isset($value)) ? $value : $_REQUEST[$attributes["name"]];
		$attributes["type"] = (isset($type)) ? $type : "password";

		if(isset($style)){
			$attributes["style"] = $style;
		}
		if(isset($class)){
			$attributes["class"] = $class;
		}
		if(isset($id)){
			$attributes["id"] = $id;
		}
    if(isset($istyle)){
      $attributes[] = "istyle=\"{$istyle}\"";
    }

		$result = array();
		foreach($attributes as $key=>$value){
			$result[] = "{$key}=\"{$value}\"";
		}
		
		return "<input " . join(" ",$result) ." />";
	}

	public function form($params){
    extract($params);
    $style = (isset($style)) ? $style : "";
    $class = (isset($class)) ? $class : "";
    $id = (isset($id)) ? $id : "";
    
    $option = "";
    
    if($multipart){
      $option .= " enctype=\"multipart/form-data\"";
    }
    if($method){
      $option .= " method=\"{$method}\"";
    }else{
      $option .= " method=\"GET\"";
    }

    if(!empty($name)){
      $option .= " name=\"{$name}\"";
    }
    if(!empty($id)){
      $option .= " id=\"{$id}\"";
    }
    if(!empty($style)){
      $option .= " style=\"{$style}\"";
    }
    if(!empty($class)){
      $option .= " class=\"{$class}\"";
    }
    
    return "<form action=\"{$action}\"" . $option . ">";
	}

	public function selected_if($params){
		if($params == true){
			return "selected";
		}
	}

	public function checked_if($params){
		if($params == true){
			return "checked";
		}
	}

	public function options($params){
    extract($params);
    $selected = (isset($selected)) ? $selected : "";
    $result = "";
    if(is_array($options)){
      foreach($options as $key => $item){
        if(strcmp($key,$selected) === 0){
          $result .= "<option value=\"{$key}\" selected>{$item}</option>\r\n";
        }else{
          $result .= "<option value=\"{$key}\">{$item}</option>\r\n";
        }
      }
    }
    return $result;
	}

	public function select_year($params){
    extract($params);
    $result = array();
    $name = (isset($name)) ? $name : "year";
    $time = (isset($time)) ? strtotime($time) : $_SERVER['REQUEST_TIME'];
    $start = (isset($start)) ? $start : date("Y");
    $end =  (isset($end)) ? $end : $start;
    if(strpos($end,"+")!==false){
      $end = date("Y")+(int)ltrim($end,"+");
    }
    $style = (isset($style)) ? $style : "";
    $class = (isset($class)) ? $class : "";
    $id = (isset($id)) ? $id : "";
    
    $option = "";
    
    if(!empty($id)){
      $option .= " id=\"{$id}\"";
    }
    if(!empty($style)){
      $option .= " style=\"{$style}\"";
    }
    if(!empty($class)){
      $option .= " class=\"{$class}\"";
    }

    $__year = date("Y",$time);
    for($i = $start;$i<=$end;$i++){
      if($__year == $i){
        $result[] = "<option value=\"{$i}\" selected>{$i}</option>";
      }else{
        $result[] = "<option value=\"{$i}\">{$i}</option>";
      }
    }
    return "<select name=\"{$name}\"{$option}>" . join("",$result) . "</select>";		
	}

	public function select_month($params){
    extract($params);
    $name = (isset($name)) ? $name : "month";
    $time = (isset($time)) ? strtotime($time)  : $_SERVER['REQUEST_TIME'];
    $style = (isset($style)) ? $style : "";
    $class = (isset($class)) ? $class : "";
    $id = (isset($id)) ? $id : "";
    
    $option = "";
    
    if(!empty($id)){
      $option .= " id=\"{$id}\"";
    }
    if(!empty($style)){
      $option .= " style=\"{$style}\"";
    }
    if(!empty($class)){
      $option .= " class=\"{$class}\"";
    }

    $__month = date("m",$time);
    for($i = 1;$i<=12;$i++){
      if($__month == $i){
        $result[] = "<option value=\"{$i}\" selected>{$i}</option>";
      }else{
        $result[] = "<option value=\"{$i}\">{$i}</option>";
      }
    }
    return "<select name=\"{$name}\"{$option}>" . join("",$result) . "</select>";
	}
	public function select_day($params){
    extract($params);
    $name = (isset($name)) ? $name : "day";
    $time = (isset($time)) ? strtotime($time)  : $_SERVER['REQUEST_TIME'];
    $style = (isset($style)) ? $style : "";
    $class = (isset($class)) ? $class : "";
    $id = (isset($id)) ? $id : "";
    
    $option = "";
    
    if(!empty($id)){
      $option .= " id=\"{$id}\"";
    }
    if(!empty($style)){
      $option .= " style=\"{$style}\"";
    }
    if(!empty($class)){
      $option .= " class=\"{$class}\"";
    }

    $__day = date("d",$time);
    for($i = 1;$i<=31;$i++){
      if($__day == $i){
        $result[] = "<option value=\"{$i}\" selected>{$i}</option>";
      }else{
        $result[] = "<option value=\"{$i}\">{$i}</option>";
      }
    }
    return "<select name=\"{$name}\"{$option}>" . join("",$result) . "</select>";
	}

	public function select_hours($params){
    extract($params);
    $name = (isset($name)) ? $name : "hours";
    $time = (isset($time)) ? strtotime($time)  : $_SERVER['REQUEST_TIME'];
    $style = (isset($style)) ? $style : "";
    $class = (isset($class)) ? $class : "";
    $id = (isset($id)) ? $id : "";
    
    $option = "";
    
    if(!empty($id)){
      $option .= " id=\"{$id}\"";
    }
    if(!empty($style)){
      $option .= " style=\"{$style}\"";
    }
    if(!empty($class)){
      $option .= " class=\"{$class}\"";
    }

    $__hour = date("H",$time);
    for($i = 0;$i<=23;$i++){
      if($__hour == $i){
        $result[] = "<option value=\"{$i}\" selected>{$i}</option>";
      }else{
        $result[] = "<option value=\"{$i}\">{$i}</option>";
      }
    }
    return "<select name=\"{$name}\"{$option}>" . join("",$result) . "</select>";		
	}

	public function select_minutes($params){
    extract($params);
    $name = (isset($name)) ? $name : "minutes";
    $time = (isset($time)) ? strtotime($time)  : $_SERVER['REQUEST_TIME'];
    $style = (isset($style)) ? $style : "";
    $class = (isset($class)) ? $class : "";
    $id = (isset($id)) ? $id : "";
    $cycle = (isset($cycle)) ? $cycle : 1;
    
    $option = "";
    
    if(!empty($id)){
      $option .= " id=\"{$id}\"";
    }
    if(!empty($style)){
      $option .= " style=\"{$style}\"";
    }
    if(!empty($class)){
      $option .= " class=\"{$class}\"";
    }

    $__minutes = date("i",$time);
    if($cycle > 1){
      $__minutes = (int)($__minutes / $cycle);
    }

    for($i = 0;$i<60/$cycle;$i++){
      $x = $i*$cycle;
      if($__minutes == $i){
        $result[] = "<option value=\"{$x}\" selected>{$x}</option>";
      }else{
        $result[] = "<option value=\"{$x}\">{$x}</option>";
      }
    }
    return "<select name=\"{$name}\"{$option}>" . join("",$result) . "</select>";
	}


	public function hidden($params){
		extract($params);
		$attributes = array();
		$attributes["name"] = $name;
		$attributes["value"] = (isset($value)) ? $value : $_REQUEST[$attributes["name"]];
		$attributes["type"] = "hidden";

		if(isset($style)){
			$attributes["style"] = $style;
		}
		if(isset($class)){
			$attributes["class"] = $class;
		}
		if(isset($id)){
			$attributes["id"] = $id;
		}
		$result = array();
		foreach($attributes as $key=>$value){
			$result[] = "{$key}=\"{$value}\"";
		}
		
		return "<input " . join(" ",$result) ." />";
	}

	public function checkboxes($params){
		extract($params);
    $selected = (isset($selected)) ? $selected : array();
    $result = "";
    if(is_array($options)){
      foreach($options as $key => $item){
        if(in_array($key,(array)$selected)){
          $result .= "<label><input type=\"checkbox\" value=\"{$key}\" name=\"{$name}\" checked />{$item}</label>\r\n";
        }else{
          $result .= "<label><input type=\"checkbox\" value=\"{$key}\" name=\"{$name}\">{$item}</label>\r\n";
        }
      }
    }
    return $result;
	}
	

	public function radio($params){
		extract($params);
    $selected = (isset($selected)) ? $selected : "";

    $result = "";
    if(is_array($options)){
      foreach($options as $key => $item){
        if(strcmp($key,$selected) === 0){
          $result .= "<label><input type=\"radio\" value=\"{$key}\" name=\"{$name}\" checked />{$item}</label>\r\n";
        }else{
          $result .= "<label><input type=\"radio\" value=\"{$key}\" name=\"{$name}\">{$item}</label>\r\n";
        }
      }
    }
    return $result;
	}

	public function select($params){
		extract($params);
    $selected = (isset($selected)) ? $selected : "";
    $result = "";
    if(is_array($options)){
      foreach($options as $key => $item){
        if(strcmp($key,$selected) === 0){
          $result .= "<option value=\"{$key}\" selected>{$item}</option>\r\n";
        }else{
          $result .= "<option value=\"{$key}\">{$item}</option>\r\n";
        }
      }
    }

    $attributes = array();
		if(isset($style)){
			$attributes["style"] = $style;
		}
		if(isset($class)){
			$attributes["class"] = $class;
		}
		if(isset($id)){
			$attributes["id"] = $id;
		}
		$r = array();
		foreach($attributes as $key=>$value){
			$r[] = "{$key}=\"{$value}\"";
		}
		$attr = " " . join(" ",$r);

    
    return "<select name=\"{$name}\"{$attr}>" . $result . "</select>";
    
	}

	public function submit($params){
		extract($params);
		$attributes = array();
		$attributes["name"] = $name;

		if(isset($style)){
			$attributes["style"] = $style;
		}
		if(isset($class)){
			$attributes["class"] = $class;
		}
		if(isset($value)){
			$attributes["value"] = $value;
		}
		if(isset($id)){
			$attributes["id"] = $id;
		}
		$result = array();
		foreach($attributes as $key=>$value){
			$result[] = "{$key}=\"{$value}\"";
		}
		$attr = join(" ",$result);
		return "<input type=\"submit\" {$attr} />";
	}
	
	public function input($params){
		extract($params);
		$attributes = array();
		$attributes["name"] = $name;
		$attributes["value"] = (isset($value)) ? $value : $_REQUEST[$attributes["name"]];
		$attributes["type"] = (isset($type)) ? $type : "text";

		if(isset($style)){
			$attributes["style"] = $style;
		}
		if(isset($class)){
			$attributes["class"] = $class;
		}
		if(isset($id)){
			$attributes["id"] = $id;
		}
    if(isset($istyle)){
      $attributes[] = "istyle=\"{$istyle}\"";
    }
		$result = array();
		foreach($attributes as $key=>$value){
			$result[] = "{$key}=\"{$value}\"";
		}
		
		return "<input " . join(" ",$result) ." />";
	}

	public function textarea($params){
		extract($params);
		$attributes = array();
		$attributes["name"] = $name;
		$_value = (isset($value)) ? $value : $_REQUEST[$attributes["name"]];

		if(isset($style)){
			$attributes["style"] = $style;
		}
		if(isset($class)){
			$attributes["class"] = $class;
		}
		if(isset($id)){
			$attributes["id"] = $id;
		}
    if(isset($istyle)){
      $attributes[] = "istyle=\"{$istyle}\"";
    }
		$result = array();
		foreach($attributes as $key=>$value){
			$result[] = "{$key}=\"{$value}\"";
		}
		$attr = join(" ",$result);
		return "<textarea {$attr}>{$_value}</textarea>";
	}
	
	public function file($params){
		extract($params);
		$attributes = array();
		$attributes["name"] = $name;
		$attributes["type"] = "file";

		if(isset($style)){
			$attributes["style"] = $style;
		}
		if(isset($class)){
			$attributes["class"] = $class;
		}
		if(isset($id)){
			$attributes["id"] = $id;
		}
		$result = array();
		foreach($attributes as $key=>$value){
			$result[] = "{$key}=\"{$value}\"";
		}
		
		return "<input " . join(" ",$result) ." />";
	}

}