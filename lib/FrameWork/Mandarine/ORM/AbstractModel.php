<?php
namespace Mandarine\ORM;

abstract class AbstractModel{
  const Cascade_Class      = "\Mandarine\ORM\Record";
  const Cascade_Collection = "\Mandarine\ORM\Collection";
  protected $environment;
  protected $table;
  protected $order;
  protected $limit;
  
  const HYDRATE_OBJECT = 0x01;
  const HYDRATE_ASSOC = 0x02;
  const HYDRATE_NONE = 0x03;
  
  public function clear_limit(){
    unset($this->limit);
  }
  
  public function limit($limit){
    $this->limit = $limit;
    return $this;
  }
  
  public function has_autoincrement(){
    $structure = $this->getStructure();
    foreach($structure["columns"] as $column){
      if($column['autoincrement'] === true){
        return true;
      }
    }
    return false;
  }
  
  public function order($order){
    $this->order[] = $order;
    return $this;
  }

  public function __construct($table_name,$environment_hash){
    $this->environment = $environment_hash;
    $this->table = $table_name;
  }

  public function getColumn($table_name = null){
    $structure = $this->getStructure($table_name);
    return $structure["columns"];
  }

  public function getStructure($table_name = null){
    if(empty($table_name)){$table_name = $this->table;}
    return \Mandarine\ORM::getInstance($this->environment)->loadStructure($table_name);
  }
  
  public function getPrimaryKey(){
    $tmp = array();
    $columns = $this->getColumn();

    foreach($columns as $name=>$attribute){
      if($name[0] == "_"){
        continue;
      }
      if(isset($attribute["primary"]) && $attribute["primary"]){
        $tmp[$name] = true;
      }
    }
    return $tmp;
  }

  public function getName(){
    return $this->table;
  }
  
  public function getCascadeCollection(){
    $structure = $this->getStructure();
    if(isset($structure["cascading"]["collection"])){

      return $structure["cascading"]["collection"];
    }else{
      $class_name = "Collection_" . \Mandarine\ORM::Cameraized($this->getName());
      if(class_exists($class_name)){
        return $class_name;
      }else{
      	return self::Cascade_Collection;
      }
    }
  }
  
  public function getCascadeRecord(){
    $structure = $this->getStructure();
    if(isset($structure["cascading"]["record"])){
      return $structure["cascading"]["record"];
    }else{
      $class_name = "Record_" . \Mandarine\ORM::Cameraized($this->getName());
      
      if(class_exists($class_name)){
        return $class_name;
      }
    }
  }
  
  public function hasCascade(){
    $structure = $this->getStructure();
    if(isset($structure["cascading"])){
      return true;
    }else{
      return false;
    }
  }
  
  public function getKeys(){
    $tmp = array();
    $structure = $this->getStructure();
    foreach($structure["columns"] as $name=>$attribute){
      if($name[0] != "_"){
        $tmp[] = $name;
      }
    }
    return $tmp;
  }
  
  public function getRelations(){
    $structure = $this->getStructure();
    if(isset($structure["relations"])){
      return $structure["relations"];
    }else{
      return false;
    }
  }
  
  public function create_select_query($where = array(),$limit=0){
    $select = array();

    $relations = $this->getRelations();
    $query = new Query();
    $select[$this->table] = $this->getKeys();

    $__where = array();
    foreach($where as $key=>$value){
      $__where[] = $key . " = " . Cacophony_Query::escape($value);
    }

    $query = $query->from($this->table)->where(join(" and ",$__where));

    if($limit){
      $query->limit($limit);
    }
    if((bool)$this->order){
      $order = join(", ",$this->order);
      $query->order($order);
    }

    if((bool)$relations){
      foreach((array)$relations as $table=>$relation){
        $join_table = \Mandarine\ORM::getModel($table);
        $query->join($table);
        $select[$table] = $join_table->getKeys();
      }
    }

    $target = array();
    foreach($select as $table_name=>$columns):
      foreach($columns as $column){
        $target[] = "{$table_name}.{$column}";
      }
    endforeach;

    return $query->select( join(', ', $target) )->freeze();
  }


  public function hydration(&$record,$fields,$row,$relations=array(),AbstractRecord &$last = null){
    foreach($fields as $offset=>$field):
      $name = $field->name;
    	$tmpname = "__" . $name  . "__";
      if($this->table == $field->orgtable || empty($field->orgtable)){
        $record->$name = $row[$offset];
        $record->$tmpname = $row[$offset];
      }else{
        $child = $field->orgtable;
        if(!isset($record->$child)){$record->appendChild($child);}
        $record->$child->$name = $row[$offset];
      }
    endforeach;

    $relations = $this->getRelations();    
    foreach((array)$relations as $name=>$relation){
      if(isset($relation["type"]) && $relation["type"] == "many"){
        $child = $record->$name;
        unset($record->$name);
        $table = \Mandarine\ORM::getModel($name);

		    $collection_class = ($collection_class = $table->getCascadeCollection()) ? $collection_class: $table->Cascade_Collection;

        $record->$name = new $collection_class($table);
        $record->$name->append($child);
      }
    }

    if( $last ){
      if($last->compare($record)){
        foreach($relations as $name=>$relation){
          if($relation["type"] == "many"){
            $last->$name->merge($record->$name);
          }
        }
        $record = null;
      }
    }
  }

  public function find_one($where = array()){
    $sql = $this->create_select_query($where,1);
    $result = \Mandarine\ORM::getInstance($this->environment)->mysqli->query($sql);

    if($error = \Mandarine\ORM::getInstance($this->environment)->mysqli->error){
      throw new Exception($error);
    }
    if($result->num_rows  > 0){
  		$fields = $result->fetch_fields();

      $record_class = ($record_class = $this->getCascadeRecord()) ? $record_class : self::Cascade_Class;
      $record = new $record_class($this);

      $this->hydration($record,$fields,$result->fetch_row());
      $result->close();

      return $record;
    }else{
      return false;
    }
  }
  
  public function find_assoc($where = array()){
    $collections = $this->find($where);
    $array = array();

    $pri = $this->getPrimaryKey();
    $keys = array_keys($pri);
    $key = array_shift($keys);

    foreach($collections as $item){
      $array[$item->$key] = $item->toArray();
    }
    return $array;
  }
  
  public function delete($where = array()){
    $select = array();

    $relations = $this->getRelations();
    $query = new Cacophony_Query();

    $__where = array();
    foreach($where as $key=>$value){
      $__where[] = $key . " = " . Cacophony_Query::escape($value);
    }

    $query = $query->where(join(" and ",$__where));

    if($limit){
      $query->limit($limit);
    }

    $sql = $query->delete($this->table)->freeze();

    $result = \Mandarine\ORM::getInstance($this->environment)->mysqli->query($sql);
    if($error = \Mandarine\ORM::getInstance($this->environment)->mysqli->error){
      throw new Exception($error);
    }
    return $result;
  }

  public function find($where=array(),$hydrate_mode = self::HYDRATE_OBJECT){
    $start = 
    $limit = null;
    if(!empty($this->limit)){
      $limit = $this->limit;
      $this->clear_limit();
    }
    $sql = $this->create_select_query($where,$limit);

		$instance = \Mandarine\ORM::getInstance($this->environment);
    $result = $instance->mysqli->query($sql);

    if($error = $instance->mysqli->error){
      throw new Exception($error);
    }

    if($result->num_rows < 1){
      $result->close();
      return false;
    }

		$fields = $result->fetch_fields();
    $collection_class = ($collection_class = $this->getCascadeCollection()) ? $collection_class: self::Cascade_Collection;
    $collection = new $collection_class($this);

    $record_class = ($record_class = $this->getCascadeRecord()) ? $record_class : self::Cascade_Class;
    $last = null;
    $relations = $this->getRelations();
		$__record_class = new $record_class($this);
    while($row = $result->fetch_row()){
      $record = clone $__record_class;
      $this->hydration($record,$fields,$row,$relations,$last);

      if($record){
        $collection->append($record);
        $last = $record;
      }
    }
    $result->close();
    
    if(count($collection)){
      return $collection;
    }else{
      return false;
    }
  }

  public function create(\Closure $closure = null){
    $record_class = ($record_class = $this->getCascadeRecord()) ? $record_class : self::Cascade_Class;
    $record = new $record_class($this);

    if($closure instanceof \Closure ){
      $closure($record);
    }
    return $record;
  }
}