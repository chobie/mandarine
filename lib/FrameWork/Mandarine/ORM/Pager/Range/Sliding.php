<?php
namespace Mandarine\ORM\Pager\Range;

class Sliding{
  private $pager;
  public $options;

  public function __construct($options=array()){
    if(is_array($options)){
      $this->options = $options;
    }
  }

  public function setPager(Cacophony_Pager &$pager){
    $this->pager = &$pager;
  }
  
  public function setOptions(array $options = array()){
    $this->options = $options;
  }
  
  public function rangeAroundPage(){
    $curPage = $this->pager->getCurPage();
    $totalPage = $this->pager->getTotalPage();
		$chunk = $this->options["chunk"];

		$center = ceil($chunk/2);

		$range_start = (($curPage - $center) < 1)  ? 1 : $curPage-$center+1;
		$range_end = min(($range_start-1) + $chunk,$totalPage);

		if( ($range_end-$range_start+1) < $chunk ){
			$range_start = $range_end - $chunk+1;
		}
    if($range_start < 1){
      $range_start = 1;
    }

    $range = array();
    for($i = $range_start; $i <= $range_end; $i++){
      if($i == $curPage){
        $range[] = array("page"=>$i,"curpage"=>true);
      }else{
        $range[] = array("page"=>$i);
      }
    }
    
    return $range;
  }
}