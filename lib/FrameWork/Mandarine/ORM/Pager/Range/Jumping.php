<?php
namespace Mandarine\ORM\Pager\Range;

class Jumping{
  private $pager;
  public $options;

  public function __construct($options=array()){
    if(is_array($options)){
      $this->options = $options;
    }
  }

  public function setPager(Cacophony_Pager &$pager){
    $this->pager = &$pager;
  }
  
  public function setOptions(array $options = array()){
    $this->options = $options;
  }
  
  public function rangeAroundPage(){
    $curPage = $this->pager->getCurPage();
    $totalPage = $this->pager->getTotalPage();
    if($curPage < 1){
      throw new Exception("Invalid Range");
    }
    if($curPage > $totalPage){
      throw new Exception("Invalid Range");
    }

    $chunk = $this->options["chunk"];

    if(($res = $curPage%$chunk)>0){
      if($curPage === 1){
        $startPage = 1;
      }else{
        $startPage = $curPage-$res+1;
        if($startPage == 0){$startPage = 1;}
      }
    }else if($curPage%$chunk == 0){
      $startPage = $curPage-$chunk+1;
    }else{
      $startPage = $curPage;
    }
    $end = $chunk+$startPage;
    if($end > $totalPage){
      $end =$totalPage;
    }
    $range = array();
    for($i=$startPage;$i<$end;$i++){
      if($i == $curPage){
        $range[] = array("page"=>$i,"curpage"=>true);
      }else{
        $range[] = array("page"=>$i);
      }
    }
    
    return $range;
  }
}