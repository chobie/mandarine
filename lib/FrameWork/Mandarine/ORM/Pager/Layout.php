<?php
namespace Mandarine\ORM\Pager;

class Layout{
  public $pager;
  public $range;
  public $url = "/{%page_number}";
  public $template = "<a href=\"%url\">[%page]</a>";
  public $selected_template = "[<b>%page</b>]";
	public $previous_template  = '<a href="%url">&lt;&lt;previous</a>&nbsp;';
	public $next_template  = '&nbsp;<a href="%url">next&gt;&gt;</a>';

	public function getPagerCurrentPage(){
		return $this->pager->getCurPage();
	}

	public function getPagerTotalCount(){
		return $this->pager->getNumResults();
	}
  public function getPagerTotalPage(){
    return $this->pager->getTotalPage();
  }

  public function getPagerResult(){
    return $this->pager->getResult();
  }
  
  public function execute(){
    $this->pager->execute();
  }
  
  public function __construct($pager,$range,$url){
    $this->pager = &$pager;
    $this->range = &$range;
    $this->range->setPager(&$pager);

    $this->url = $url;
  }
  

  public function setNextTemplate($url){
    $this->next_template = $url;
  }
  public function setPreviousTemplate($url){
    $this->previous_template = $url;
  }
  public function setSelectedTemplate($url){
    $this->selected_template = $url;
  }
  public function setTemplate($url){
    $this->template = $url;
  }
  
  
  public function applyURL($page_number,$options=array()){
  	$options = array_merge($options,array("page_number"=>$page_number));

    $url = $this->url;
		$result = "";
		while(preg_match("/\{%(?<key>[a-zA-Z][a-zA-Z0-9_-]*)\}/",$url,$match,PREG_OFFSET_CAPTURE)){
			if(isset($options[$match['key']['0']])){
				$result .= substr($url,0,$match['0']['1']) . rawurlencode($options[$match['key']['0']]);
			}else{
				$result .= substr($url,0,$match['0']['1']);
			}
			$url = substr($url,$match['0']['1']+strlen($match['0']['0']));
		}

		if(!empty($url)){
			$result .= $url;
		}

    return $result;
  }
  
  public function applySelectedTemplate($range,$request=array()){
    return $this->applyTemplate($range,$request,"selected_template");
  }
  
  public function applyTemplate($range,$request=array(),$template_name="template"){
    $template = $this->$template_name;
    $template = str_replace("%url",$this->applyURL($range,$request),$template);
    $template = str_replace('%page',$range,$template);
    return $template;
  }
  
  
  public function fetch($request=array()){
    $range = $this->range->rangeAroundPage();

    $result = "";
		if($this->getPagerCurrentPage()>1){
			$result .= $this->applyTemplate($this->getPagerCurrentPage()-1,$request,"previous_template");
		}
    foreach($range as $key=>$item){
      if(isset($item['curpage'])){
        $result .= $this->applySelectedTemplate($item['page'],$request);
      }else{
        $result .= $this->applyTemplate($item['page'],$request);
      }
    }
		if($this->getPagerTotalPage() > 0 && $this->getPagerCurrentPage() != $this->getPagerTotalPage()){
			$result .= $this->applyTemplate($this->getPagerCurrentPage()+1,$request,"next_template");
		}
    return $result;
  }
}