<?php
namespace Mandarine\ORM;

class Pager{
  protected $executed = false;
  private $result;
  public $query;

  public $curPage;
  public $perPage;
  public $firstPage = 1;
  public $lastPage;
  public $totalPage;
  public $numResult;

  public function getResult(){
    return $this->result;
  }

  public function getTotalPage(){
    return $this->totalPage;
  }

  public function getCurPage(){
    return $this->curPage;
  }

  private function setExecuted(){
    $this->executed = true;
  }

  public function getNumResults(){
    if($this->getExecuted()){
      return $this->numResult;
    }else{
      return 0;
    }
  }

  public function getFirstPage(){
    return $this->firstPage;
  }
  
  public function getLastPage(){
    return $this->lastPage;
  }
  
  public function haveToPaginate(){
    if($this->totalPage  > 1){
      return true;
    }else{
      return false;
    }
  }
  
  public function getRange($rangeStyle = "Sliding",$options = array()){
    $range_class = "Cacophony_Pager_Range_" . $rangeStyle;

    $range = new $range_class($options);
    $range->setPager($this);
    return $range;
  }

  public function __construct($query,$curPage,$perPage){
    $this->query = $query;
    $this->curPage = max($curPage,1);
    $this->perPage = $perPage;
  }
  
  public function getExecuted(){
    return ($this->executed === true) ? true : false;
  }
  
  
  public function execute($method_params = array()){
    $this->query->use_calc_found_rows();
    $this->query->limit($this->perPage,($this->curPage*$this->perPage-$this->perPage));

    try{
      $this->result = $this->query->execute();
      $this->numResult = $this->query->get_found_rows();
      $this->totalPage = ceil($this->numResult / $this->perPage);

      $this->setExecuted();
    }catch(Exception $e){
      echo $e;
      throw $e;
    }
  }
}