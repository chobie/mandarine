<?php
namespace Mandarine\ORM;

abstract class AbstractRecord implements \Serializable{
  protected $__table__;
  protected $__has_child__;
  
  public function getBehaviors(){
    $st = $this->getStructure();
    if(isset($st["behavior"])){
      return $st["behavior"];
    }else{
      return array();
    }
  }

  public function getTable(){
    return $this->__table__;
  }

  public function getStructure(){
    return $this->__table__->getStructure();
  }
  public function is_empty(){
    foreach($this->getFields() as $field){
      if(!empty($record->$field)){
        return false;
      }
    }
    return true;
  }

  public function getChildren(){
    return array_keys($this->__table__->getRelations());
  }
  
  public function compare($record){
    foreach($this->getFields() as $field){
      if(isset($record->$field) && $this->$field != $record->$field){
        return false;
      }
    }
    return true;
  }

  final public function serialize(){
    return $this->toArray();
  }
  
  final public function unserialize($data){
    $this->fromArray($data);
  }
  
  public function __init__(){
  }
  
  final public function __construct(AbstractModel &$table){
    $this->__table__     = $table;

    $structure = $table->getStructure();
    foreach($table->getKeys() as $property){
      if(isset($structure["columns"][$property]["default"])){
        $this->$property = $structure["columns"][$property]["default"];
      }else{
        $this->$property = null;
      }
    }

    $this->__has_child__ = array();
    $this->__init__();
  }

  final public function getFields(){
    return $this->__table__->getKeys();
  }
  
  final public function fromArray(array $array){
    foreach($this->getFields() as $field){
      if(isset($array[$field])){
        $this->$field = $array[$field];
      }
    }

    $keys = $this->__table__->getRelations();
    if(is_array($keys)){
      $relations = array_keys($keys);
      foreach($relations as $table){
        $table_class = \Mandarine\ORM::getModel($table);
        if(isset($array[$table])){
          $record = $table_class->create($array[$table]);
          $this->$table = $record;
          $this->__has_child__[] = $table;
        }
      }
    }
  }
  
  final public function toYaml(){
    return yaml_emit($this->toArray());
  }

  public function set($field,$value){
    $this->$field = $value;
  }

  public function __set($field,$value){
    //if(!( isset($this->__table__) && $this->__table__->getStructure() ) ||
    //   isset($this->__table__->getStructure()->$field) ||
    //   isset($this->__table__->getStructure()->getRelations()->$field)
    //){
      $this->$field = $value;
    //}else{
      //throw new Exception("can't set undefined value `{$field}` to {$this->__table__->getName()}");
    //}
  }

  public function toArray(){
    $array = array();
    
    foreach($this as $field=>$attributes){
      if($field[0] != "_"){
        $array[$field] = $this->$field;
      }
    }

    foreach($this as $child=>$object){
    	if(method_exists($object,"toArray")){
    		$array[$child] = $object->toArray();
    	}
    }

    return $array;
  }
  
  public function before_insert(){}
  
  public function after_insert(){}
  
  final public function insert($bSaveChildren = true){
    foreach($this->getBehaviors() as $act => $behavior){
      foreach($behavior as $when => $params){
        if($when == "insert"){
          $classname = "Cacophony_Behavior_" . ucwords($act);
          if(class_exists($classname)){
            call_user_func_array(array($classname,"apply"),array(&$this->$params[0]));
          }else{
            throw new Exception("Can't find behavior `{$classname}`");
          }
        }
      }
    }

    if($this->before_insert() === false){
      throw new Exception();
    }

    $instance = \Mandarine\ORM::getInstance();
    $query = new Query();
    $fields = array();
    $field_values = array();
    $field_type = "";
    $auto_increment_key = "";

    foreach($this->__table__->getColumn() as $field=>$attributes){
      if($attributes["autoincrement"]){
        $auto_increment_key = $field;
      }
      if(!is_null($this->$field) && !$attributes["autoincrement"]){
        $fields[] = $field;
        $field_values[] = $this->$field;
        //$field_type .= $query->getParam($this->$field);
      }
    }

    $query->insert( $this->__table__->getName() )
      ->values(join(", ",$fields),$field_values);

//    $sql = $query->freeze();
/*
    $stmt = $instance->mysqli->prepare($sql);
    $types = array();
    $types[] = $field_type;

    foreach($query->args as $key=>$arg){
    	$types[] = &$query->args[$key];
    }
    call_user_func_array(array($stmt,"bind_param"),$types);
*/  
    
    //if($result = $stmt->execute()){
    if($result = $query->execute()){
      if($auto_increment_key && !$this->$auto_increment_key){
	      $id = $instance->mysqli->insert_id;
        $this->$auto_increment_key = $id;
      }

      if((bool)$this->__has_child__ && $bSaveChildren){
        foreach($this->__has_child__ as $child){
          $child_table = \Mandarine\ORM::getModel($child);
          $keys = $child_table->getPrimaryKey();
          $keys = array_keys($keys);
          $key = array_shift($keys);

          $this->$child->$key = $id;
          $this->$child->insert();
        }
      }
      
      if($this->after_insert() === false){
        throw new Exception();
      }
      if($auto_increment_key && !$this->$auto_increment_key){
      	return $id;
      }else{
      	return true;
      }
    }else{
      throw new Exception($instance->mysqli->error);
    }
  }
  
  
  public function before_save(){
  }
  public function after_save(){
  }

  final public function save($bSaveChildren = true){
    //$structure = $this->__table__->getColumn();
    $primary = array_keys($this->__table__->getPrimaryKey());
    
    // primary keyがなければinsertする。
    $q = new Query();
    $where = array();
    $_values = array();
    foreach($primary as $k){
      if(!empty($this->$k)){
        $where[] = $k . " = ?";
        $_values[] = $this->$k;
      }
    }
    if(!count($where)){
      return $this->insert($bSaveChildren);
    }
    
		//primary keyで検索してレコードがなければinsertする
    $q->from($this->__table__->getName())->where(join(" and ", $where),$_values)->select("1");
    if(!$q->execute()){
      return $this->insert();
    }

    foreach($this->getBehaviors() as $act => $behavior){
      foreach($behavior as $when => $params){
        if($when == "update"){
          $classname = "Mandarine\Behavior\\" . ucwords($act);
          if(class_exists($classname)){
            call_user_func_array(array($classname,"apply"),array(&$this->$params[0]));
          }else{
            throw new Exception("Can't find behavior `{$classname}`");
          }
        }
      }
    }
    if($this->before_save() === false){ throw new Exception(); }

//    $instance = Cacophony::getInstance();

    $query = new Query();
		// updateのset句の作成

    $fields = $field_values = array();
    $field_type = "";
    foreach($this->__table__->getColumn() as $field=>$attributes){
      if(isset($this->$field) && !in_array($field,$primary)){
      	$check_field = "__" . $field . "__";
      	if(isset($this->$check_field) && $this->$check_field != $this->$field){
	        $fields[] = $field . " = ?";
	        $field_values[] = $this->$field;
      	}else if(isset($this->$check_field) && $this->$check_field == $this->$field){
      		// 一緒なのでなにもしない
      	}else{
	        $fields[] = $field . " = ?";
	        $field_values[] = $this->$field;
	      }
      }
      //$field_type .= $query->getParam($this->$field);
    }
		//レコードが変わってないのでセーブしない
    if(!count($fields)){
    	return false;
    }

		// updateのwehere句の作成
    $pris = $pri_value = array();
    foreach($this->__table__->getPrimaryKey() as $pri=>$dummy){
      $pris[]      = $pri . " = ?";
      $pri_value[] = $this->$pri;
      //$field_type .= $query->getParam($this->$pri);
    }

    $query->update( $this->__table__->getName() )->set(join(", ",$fields),$field_values)->where(join(" and ",$pris),$pri_value);
    //$sql = $query->freeze();
    $result = $query->execute();
		
    //$stmt = $instance->mysqli->prepare($sql);
    //$types = array();
    //$types[] = $field_type;
    //foreach($query->args as $arg){$types[] = $arg;}
    //call_user_func_array(array($stmt,"bind_param"),$types);
    //if($result = $stmt->execute()){

    if($result){
      if($this->after_save() === false){
        throw new Exception();
      }

	    if($bSaveChildren){
	      foreach($this->__has_child__ as $child){
	        if($this->$child instanceof AbstractRecord ){
	          $this->$child->save();
	        }
	      }
	    }

      return $result;
    }else{
      return $result;
    }
  }
  
  public function before_delete(){
  }
  
  public function after_delete(){
  }

  final public function delete(){
    if($this->before_delete() === false){
      throw new Exception();
    }

    $instance = \Mandarine\ORM::getInstance();
    $query = new Query();

    $field_type = "";
    $fields = $field_values = array();

    $pris = $pri_value = array();
    foreach($this->__table__->getPrimaryKey() as $pri=>$dummy){
      $pris[]      = $pri . " = ?";
      $pri_value[] = $this->$pri;
      $field_type .= $query->getParam($this->$pri);
    }

    $query->delete($this->__table__->getName())->where(join(" and ",$pris),$pri_value);
    $result = $query->execute();

/*
    $stmt = $instance->mysqli->prepare($sql);
    $types = array();

    $types[] = $field_type;
    foreach($query->args as $arg){$types[] = $arg;}
    call_user_func_array(array($stmt,"bind_param"),$types);

    if($result = $stmt->execute()){
*/
		if($result){
      if($this->after_delete() === false){
        throw new Exception();
      }
      if($this->__table__->has_autoincrement()){
	      $instance->mysqli->query("alter table " . $this->__table__->getName() . " auto_increment = 1");
      }
      return $result;
    }else{
      return $result;
    }
  }
  
  final public function appendChild($table){
    $table_class = \Mandarine\ORM::getModel($table);

    $this->$table = $table_class->create();
    $this->__has_child__[] = $table;
  }
}