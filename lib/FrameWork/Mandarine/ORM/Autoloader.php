<?php
namespace Mandarine\ORM;

class Autoloader{
  public static function register(){
    spl_autoload_register(array(__CLASS__, 'loadClass'));
  }

  public static function loadClass($class)
  {
    if(strpos($class,"Mandarine")!==false){
      $class_name = str_replace('_', DIRECTORY_SEPARATOR, $class);
      if (0 === strpos($class, "Mandarine")){
        $file = $class_name .'.php';

        if(is_file($file)){
          require $file;
        }
        return true;
      }
    }
    return false;
  }
}