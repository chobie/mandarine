<?php
namespace Mandarine\ORM;

class Query{
  protected $type;
  protected $table;
  protected $query;
  protected $order;
  protected $join;
  protected $set;
  protected $where;
  protected $values;
  protected $use_calc_found_rows;
  protected $rows = 0;
  protected $duplicate;
  protected $group;
  protected $lock;
  protected $limit;

  public $args = array();

  public function get_found_rows(){
    return $this->rows;
  }
  

  public function use_calc_found_rows(){
    $this->use_calc_found_rows = true;
  }
  
  public static function escape($value){
    if(is_numeric($value)){
      return $value;
    }else if(is_null($value)){
      return $value;
    }else if(is_bool($value)){
      return $value;
    }else if(is_string($value)){
      return sprintf('"%s"', Cacophony::getInstance()->mysqli->real_escape_string($value));
    }
  }

	public static function str_count($string,$char){
		$length = strlen($string);
		$count = 0;
		for($i=0;$i<$length;$i++){
			if($string[$i] === $char){
				$count++;
			}
		}
		return $count;
	}
  
  public function duplicate($update){
    $this->duplicate = $update;
    return $this;
  }
  
  public function execute(){
    $sql = $this->freeze();
    $instance = \Mandarine\ORM::getInstance();

    if($this->type == "select"){
  		if(($qc = $this->str_count($sql,"?")) != ($ac = count($this->args))){
  			throw new Exception("parameter counter doesn't match.query has {$qc} params. but parameter has $ac.");
  		}
      
  		foreach((array)$this->args as $value){
  			$sql = substr_replace($sql,$this->escape($value),strpos($sql,"?"),1);
  		}

      $result = $instance->mysqli->query($sql);
      if($error = \Mandarine\ORM::getInstance()->mysqli->error){
        throw new Exception($error . " " . $sql);
      }

      if($result->num_rows < 1){
        $result->close();
        return false;
      }

      if($this->use_calc_found_rows){
        $found = $instance->mysqli->query("select found_rows() as rows");
        $ass = $found->fetch_assoc();
        $this->rows = $ass["rows"];
      }

      $table = \Mandarine\ORM::getModel($this->table);
  		$fields = $result->fetch_fields();

	    $collection_class = ($collection_class = $table->getCascadeCollection()) ? $collection_class: $table->Cascade_Collection;
	    $collection = new $collection_class($table);

      $record_class = ($record_class = $table->getCascadeRecord()) ? $record_class : "\Mandarine\ORM\Record";
      $last = null;
      $relations = \Mandarine\ORM::getModel($this->table)->getRelations();

      while($row = $result->fetch_row()){
        $record = new $record_class($table);
        $this->hydration($record,$fields,$row,$relations,$last);
//        $collection->append($record);

        if($record){
          $collection->append($record);
          $last = $record;
        }
      }

      $result->close();
      return $collection;
    }else{
      $stmt = $instance->mysqli->prepare($sql);
      if((bool)$this->args){
        $field_type = "";
        foreach($this->args as $_k => $arg){
          $field_type .= $this->getParam($arg,$_k);
        }
        $types = array();
        $types[] = $field_type;
        foreach($this->args as $key=>$arg){
        	$types[] = &$this->args[$key];
        }

        call_user_func_array(array($stmt,"bind_param"),$types);
      }
      return $stmt->execute();
    }
  }

  public function hydration(&$record,$fields,$row,$relations=array(),Cacophony_AbstractRecord &$last = null){
    foreach($fields as $offset=>$field):
      $name = $field->name;
      if($this->table == $field->orgtable || empty($field->orgtable)){
        $record->$name = $row[$offset];
      }else{
        $child = $field->orgtable;
        if(!isset($record->$child)){$record->appendChild($child);}

        if(!empty($row[$offset])){
          $record->$child->$name = $row[$offset];
        }
      }
    endforeach;

    $relations = \Mandarine\ORM::getModel($this->table)->getRelations();
    foreach((array)$relations as $name=>$relation){
      if(isset($relation["type"]) && $relation["type"] == "many"){
        $child = $record->$name;
        unset($record->$name);
        $table = \Mandarine\ORM::getModel($name);
		    $collection_class = ($collection_class = $table->getCascadeCollection()) ? $collection_class: $table->Cascade_Collection;

        $record->$name = new Collection($table);
        $record->$name->append($child);
      }
    }

    if( $last ){
      if($last->compare($record)){
      	if($relations){
	        foreach($relations as $name=>$relation){
	          if($relation["type"] == "many"){
	            $last->$name->merge($record->$name);
	          }
	        }
        }
        $record = null;
      }
    }
  }
  public function getParam($value,$reference = null){
    if(!empty($reference)){

      $table = \Mandarine\ORM::getModel($this->table);
      $structure = $table->getStructure();
      $ref = $this->ref[$reference];
      if(isset($structure["columns"][$ref]["type"])){
        if(preg_match("/(float|decimal|double|bigint)/i",$structure["columns"][$ref]["type"])){
          return "d";
        }else if(preg_match("/(char|text|blob|date)/i",$structure["columns"][$ref]["type"])){
          return "s";
        }else{
          return "i";
        }

      }else{
        if(is_numeric($value)){
          return (is_float($value)) ? "d" : "i";
        }else if(is_null($value)){
        	throw new Exception("bind_param must have value.");
        }else if(is_bool($value)){
          return "i";
        }else if(is_string($value)){
          return "s";
        }
      }
    }else{
      if(is_numeric($value)){
        return (is_float($value)) ? "d" : "i";
      }else if(is_null($value)){
      	throw new Exception("bind_param must have value.");
      }else if(is_bool($value)){
        return "i";
      }else if(is_string($value)){
        return "s";
      }
    }
  }

  public function insert($table){
    $this->type = "insert";
    $this->table = $table;
    return $this;
  }
  
  public static function create(){
    return new Cacophony_Query();
  }

  public function __construct(){
  }
  
  public function freeze(){
    $sql = array();
    if($this->type == "update"){
      $sql[] = "update";
      $sql[] = $this->table;
      $sql[] = "set";
      $sql[] = $this->set;
      if((bool)$this->where){
        $sql[] = "WHERE";
        $where = array();
        foreach($this->where as $key=>$value){
          $where[] = $value;
        }
        $sql[] = join(" and ",$where);
      }
      $sql = join(" ",$sql);
      return $sql;

    }else if($this->type == "delete"){
      $sql[] = "delete";
      $sql[] = "from";
      $sql[] = $this->table;
      if((bool)$this->where){
        $sql[] = "WHERE";
        $where = array();
        foreach($this->where as $key=>$value){
          $where[] = $value;
        }
        $sql[] = join(" and ",$where);
      }
      $sql = join(" ",$sql);
      return $sql;

    }else if($this->type == "insert"){
      $sql[] = "insert into";
      $sql[] = $this->table;
      $sql[] = "(" . $this->values . ")";
      $sql[] = "values";
      $sql[] = "(" . rtrim(str_repeat("?,",count($this->args)),",") . ")";
      $sql = join(" ",$sql);
      if(isset($this->duplicate)){
        $sql .= " on duplicate key update " . $this->duplicate;
        unset($this->duplicate);
      }
      return $sql;

    }else if($this->type == "select"){
      $sql[] = "SELECT";
      if($this->use_calc_found_rows){
        $sql[] = "SQL_CALC_FOUND_ROWS";
      }
      if(!$this->select){
        $sql[] = "*";
      }else{
        $sql[] = $this->select;
      }
      $sql[] = "FROM";
      $sql[] = $this->table;
      if((bool)$this->join){
        foreach($this->join as $joiner){
          $sql[] = "LEFT JOIN";
          $sql[] = $joiner;
          $sql[] = "ON";
          if(is_array($this->using[$joiner]['local'])){
            $count = count($this->using[$joiner]['local']);
            $tmp = array();
            for($i=0;$i<$count;$i++){
              $tmp[] = "{$this->table}.{$this->using[$joiner]['local'][$i]} = {$joiner}.{$this->using[$joiner]['foreign'][$i]}";
            }
            $sql[] = join(" and ",$tmp);
          }else{
            $sql[] = "{$this->table}.{$this->using[$joiner]['local']} = {$joiner}.{$this->using[$joiner]['foreign']}";
          }
        }
      }
      if((bool)$this->where){
        $sql[] = "WHERE";
        $where = array();
        foreach($this->where as $key=>$value){
          $where[] = $value;
        }
        $sql[] = join(" and ",$where);
      }

      if((bool)$this->group){
        $sql[] = "GROUP BY " . $this->group;
      }


      if((bool)$this->order){
        $sql[] = "ORDER BY " . $this->order;
      }

      if((bool)$this->limit){
        $sql[] = "LIMIT " . $this->limit[0] . ", " . $this->limit[1];
      }
      
      if((bool)$this->lock){
      	$sql[] = "FOR UPDATE";
      }

      $sql = join(" ",$sql);
      return $sql;
    }
  }
  
  public function where($where){
    if(!empty($where)){
      $args = func_get_args();
      array_shift($args);
      if(func_num_args() >1  && $args){
        foreach((array)$args as $arg){
          if(is_array($arg)){
            foreach($arg as $ar){
              $this->args[] = $ar;
            }
          }else{
            $this->args[]  = $arg;
          }
        }
      }
      $this->where[] = $where;
    }
    return $this;
  }
  
  public function locked(){
  	$this->lock = true;
  	return $this;
  }
  
  public function select($select){
    $this->type = "select";
    $this->select = $select;
    return $this;
  }
  
  public function delete($delete){
    $this->type = "delete";
    $this->table = $delete;
    return $this;
  }
  
  public function limit($offset = 0,$limit = 0){
    $this->limit = array($limit,$offset);
    return $this;
  }
  
  public function join($join, $using = array()){
    if(!($join instanceof Cacophony_Table)){
      $join = \Mandarine\ORM::getModel($join);
    }

    $name = $join->getName();
    $this->join[] = $name;

    if(!(bool)$using){
      $relations = \Mandarine\ORM::getModel($this->table)->getRelations();
      $this->using[$name] = $relations[$name];
    }else{
      $this->using[$name] = $using;
    }

    return $this;
  }


  public function group($group){
    $this->group = $group;
    return $this;
  }

  public function order($order){
    $this->order = $order;
    return $this;
  }

  public function from($table){
    $this->table = $table;
    return $this;
  }
  
  public function update($table){
    $this->table = $table;
    $this->type = "update";
    return $this;
  }
  
  public function values($values){
    if(!empty($values)){
      $args = func_get_args();
      array_shift($args);
      if(func_num_args() >1  && $args){
        foreach((array)$args as $arg){
          if(is_array($arg)){
            foreach($arg as $ar){
              $this->args[] = $ar;
            }
          }else{
            $this->args[]  = $arg;
          }
        }
      }
      $this->values = $values;

      $sets = explode(",",$values);
      if(count($sets)){
        foreach($sets as $ss){
          $this->ref[] = rtrim(trim($ss)," =?");
        }
      }
    }

    return $this;
  }

  public function set($set){
    if(!empty($set)){
      $args = func_get_args();
      array_shift($args);
      if(func_num_args() >1  && $args){
        foreach((array)$args as $arg){
          if(is_array($arg)){
            foreach($arg as $ar){
              $this->args[] = $ar;
            }
          }else{
            $this->args[]  = $arg;
          }
        }
      }
      $this->set = $set;
      $sets = explode(",",$set);
      if(count($sets)){
        foreach($sets as $ss){
          $this->ref[] = rtrim(trim($ss)," =?");
        }
      }
    }
    return $this;
  }
}
