<?php
namespace Mandarine\ORM;

class Validator{
  protected $object;
  protected $errors;
  protected $rules;

  public function remove($key){
    if(isset($this->rules[$key])){
      unset($this->rules[$key]);
    }
  }

  public function __construct(&$object){
    $this->object = $object;
    $this->errors = array();
    if($object instanceof AbstractRecord){
	    $structure = $this->object->getStructure();
	    $this->rules = $structure["columns"];
    }
  }
  
  public function add($target,$rules){
    foreach($rules as $key=>$value){
      $this->rules[$target][$key] = $value;
    }
  }
  
  public function is_valid(){
    $errors = array();
    foreach($this->rules as $type => $column){
      $value = $this->object->$type;
      foreach($column as $rule=>$condition){
        switch($rule):
          case "date":
            $value = str_replace("-","",$value);
            if(!preg_match("/[0-9]{8}/",$value,$match)){
              $errors[$type][$rule] = $condition["msg"];
              break 2;
            }else{
              $year = substr($value,0,4);
              $month = ltrim(substr($value,4,2),"0");
              $day =  ltrim(substr($value,6,2),"0");
              
              if(!(($year > 1900 && $year <= date("Y")) && ($month >= 1 && $month <= 12) && ($day >= 1 && $day <= 31))){
                $errors[$type][$rule] = $condition["msg"];
                break 2;
              }
              
            }
          break;
          case "equal":
            if($value != $condition["rule"]){
              $errors[$type][$rule] = $condition["msg"];
              break 2;
            }
          break;
          case "id":
            if(!preg_match("/[a-zA-Z][a-zA-Z0-9_-]{3,}/",$value)){
              $errors[$type][$rule] = $condition["msg"];
              break 2;
            }
          break;
          case "hiragana":
            if(!preg_match("/^(\xe3(\x81[\x81-\xbf]|\x82[\x80-\x93]|\x83\xbc))*$/",$value)){
              $errors[$type][$rule] = $condition["msg"];
              break 2;
            }
          break;
          case "katakana":
            if(!preg_match("/^(\xe3(\x82[\xa1-\xbf]|\x83[\x80-\xb6]|\x83\xbc))*$/",$value)){
              $errors[$type][$rule] = $condition["msg"];
              break 2;
            }
          break;
          case "minlength":
            $length = mb_strlen($value,"utf-8");
            if(!($length >= $condition["rule"])){
              $errors[$type][$rule] = $condition["msg"];
              break 2;
            }
          break;
          case "maxlength":
            $length = mb_strlen($value,"utf-8");
            if(!($length <= $condition["rule"])){
              $errors[$type][$rule] = $condition["msg"];
              break 2;
            }
          break;
          case "phone":
            if(!preg_match('/^0[0-9]{9,10}$/', $value)){
              $errors[$type][$rule] = $condition["msg"];
              break 2;
            }
          break;
          case "postal":
            $value = str_replace("-","",$value);
            if(!preg_match("/^\d{4,7}$/",$value)){
              $errors[$type][$rule] = $condition["msg"];
              break 2;
            }
          break;
          case "email":
            if(!preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+([\.][a-z0-9-]+)+$/i", $value)){
              $errors[$type][$rule] = $condition["msg"];
              break 2;
            }
          break;
          case "emailmobile":
            if (eregi('([[:alnum:]\S\+\$\?\.%,!#~*/:&=_-]+)@([[:alnum:]\S\+\$\?%,!#~*/:&=_-]+)\.([[:alnum:]\S\+\$\?\.%,!#~*/:&=_-]+)', $value, $dse)) {
                if (count($dse) > 2) {
                    $domain = strtolower($dse[2]. '.' .$dse[3]);
                    if (eregi('^docomo\.ne\.jp$', $domain)) {
                    } elseif(eregi('^[a-z].vodafone\.ne\.jp$', $domain) || eregi('^softbank\.ne\.jp$', $domain)) {
                    } elseif(eregi('ezweb\.ne\.jp$', $domain)) {
                    }else{
                      $errors[$type][$rule] = $condition["msg"];
				              break 2;
                    }
                }else{
                  $errors[$type][$rule] = $condition["msg"];
		              break 2;
                }
            }else{
              $errors[$type][$rule] = $condition["msg"];
              break 2;
            }
          break;
          case "past":
            $time = strtotime($value);
            if(isset($condition["than"])){
              $tmp = $condition["than"];

              $value2 = strtotime($this->object->$tmp);
              if($time >= $value2){
                $errors[$type][$rule] = $condition["msg"];
                break 2;
              }
            }else{
              if($time >= $_SERVER["REQUEST_TIME"]){
                $errors[$type][$rule] = $condition["msg"];
                break 2;
              }
            }
          break;
          case "future":
            $time = strtotime($value);
            if(isset($condition["than"])){
              $tmp = $condition["than"];
              $value2 = strtotime($this->object->$tmp);
              if($time <= $value2){
                $errors[$type][$rule] = $condition["msg"];
                break 2;
              }
            }else{
              if($time <= $_SERVER["REQUEST_TIME"]){
                $errors[$type][$rule] = $condition["msg"];
                break 2;
              }
            }
          break;
          case "unique":
				    $table = $this->object->getModel();
            if(empty($value)){
              $errors[$type][$rule] = $condition["msg"];
              break 2;
            }else if($table->find_one(array($type=>$value))){
              $errors[$type][$rule] = $condition["msg"];
              break 2;
            }
          break;
          case "nospace":
            if(preg_match("/[\s　]/",$value)){
              $errors[$type][$rule] = $condition["msg"];
              break 2;
            }
          break;
          case "htmlcolor":
          break;
          case "regexp":
            if(!preg_match($condition["rule"],$value)){
              $errors[$type][$rule] = $condition["msg"];
              break 2;
            }
          break;
          case "url":
            if(!empty($value) && !preg_match("|https?://.+|",$value)){
              $errors[$type][$rule] = $condition["msg"];
              break 2;
            }
          break;
          case "characters":
            if(!preg_match("/^[!-~]+$/",$value)){
              $errors[$type][$rule] = $condition["msg"];
              break 2;
            }
          break;
          case "range":
            if(!($value >= $condition["rule"][0] && $value <= $condition["rule"][1])){
              $errors[$type][$rule] = $condition["msg"];
              break 2;
            }
          break;
          case "required":
            $value = trim($value);
            if(!strlen($value)){
              $errors[$type][$rule] = $condition["msg"];
              break 2;
            }
          break;
          case "numeric":
            $value = trim($value);
            if(!preg_match("/^\d+$/",$value)){
              $errors[$type][$rule] = $condition["msg"];
              break 2;
            }
          break;
          case "unsigned":
            $value = trim($value);
            if($value < 0){
              $errors[$type][$rule] = $condition["msg"];
              break 2;
            }
          break;
          case "greater":
            $value = trim($value);
            $tmp = $condition["than"];

            if($value == $tmp || $value < $tmp){
              $errors[$type][$rule] = $condition["msg"];
              break 2;
            }
          break;
          case "less":
            $value = trim($value);
            $tmp = $condition["than"];
            if($value != $tmp && $value > $tmp){
              $errors[$type][$rule] = $condition["msg"];
              break 2;
            }
          break;
          case "primary":
            break;
          case "autoincrement";
            break;
          case "default";
            break;
          case "type":
          break;
        endswitch;
      }
      
    }
    
    if(count($errors)){
      $this->__errors__ = $errors;
      return false;
    }else{
      return true;
    }
  }

  public function getErrors(){
    return $this->__errors__;
  }
}