<?php
namespace Mandarine\ORM;

class AbstractCollection implements \Iterator, \Countable, \Serializable{
  protected $table;
  protected $position = 0;
  protected $array;
	protected $count = 0;
  
  public function merge(Cacophony_Collection $collection){
    foreach($collection as $item){
      $this->append($item);
    }
  }

  public function __construct(&$table){
    $this->table = $table;
  }
  
  public function serialize(){
    return $this->toArray();
  }
  
  public function unserialize($data){
    throw new \Exception("unserialize method does not support.");
  }

  public function current(){
    return $this->array[$this->position];
  }
  public function key(){
    return $this->position;
  }

  public function next(){
    ++$this->position;
  }

  public function rewind(){
    $this->position = 0;
  }

  public function valid(){
    return isset($this->array[$this->position]);
  }
  
  public function count(){
    return $this->count;
  }
  
  public function append($row){
    if((bool)$row){
      $this->array[] = $row;
      $this->count++;
      return true;
    }else{
      return false;
    }
  }
  
  public function toArray(){
    $array = array();
    foreach($this as $item){
      $array[] = $item->toArray();
    }

    return $array;
  }
  
  public function save(){
    foreach($this as $record){
      $record->save();
    }
    return;
  }
  
  public function delete(){
    $keys = array_keys($this->table->getPrimaryKey());
    if(count($keys) != 1){
      throw new Exception("multiple primary key exception");
    }

    $targets = array();
    foreach($this as $item){
      $targets[] = $item->$keys[0];
    }

    //paste
    $instance = \Mandarine\ORM::getInstance();
    $query = new \Mandarine\ORM\Query();

    $fields = array();
    $field_values = array();
    $field_type = "";

    $field_type .= $query->getParam($item->$keys[0]);
    $query->delete( $this->table->getName() )
      ->where("{$keys[0]} IN (" . join(", ", $targets) . ")");
    $sql = $query->freeze();

    $stmt = $instance->mysqli->prepare($sql);
    $types = array();

    $types[] = $field_type;
    foreach($query->args as $arg){$types[] = $arg;}
    call_user_func_array(array($stmt,"bind_param"),$types);

    if($result = $stmt->execute()){
      return $result;
    }else{
      return $result;
    }
  }
}