﻿<?php
namespace Mandarine;

class Template{
    public $compiler_class = "Mandarine\Template\Compiler";
    public $prefix = "__MandarineTemplate__";
    public $compiler;
    public $cache;
    public $dir;
    public $template;
    public $extend;
    public $evaluates;

    public function loadTemplate($filename){
        $this->template = $filename;
    }

    public function getClassName($real_path){
        return $this->prefix . md5($real_path);
    }

    public function is_fresh($file,$time){
        clearstatcache();
        return filemtime($file) < $time;
    }
    
    public function render(array $context){
        ob_start();
        $this->display($context);
        return ob_get_clean();
    }
    
    public function clear(){
        $this->extend = "";
    }

    public function Import($name,$context=array()){
        $path = $this->dir . DIRECTORY_SEPARATOR . $name;
        $classname = $this->getClassName($path);
        $compiled_path = $this->cache . DIRECTORY_SEPARATOR . $classname . ".php";

        $classname = $this->prefix . md5($path);
        if(!$this->is_compiled($path,$compiled_path)){
            $this->compile(file_get_contents($path));
        if($this->extend){
            $extend_class = $this->prefix . md5($this->dir . DIRECTORY_SEPARATOR . $this->extend);
        }else{
            $extend_class = "";
        }

        file_put_contents($compiled_path, 
            $this->compiler->make_class($name,$classname,$extend_class,""), 
            LOCK_EX);
        }
        require_once $compiled_path;

        $c = new $classname($this);
        $c->display($context);
    }

    public function __construct(array $options = array()){
        $this->cache = (isset($options["cache"])) ? realpath($options["cache"]) : realpath("./") ;
        $this->dir = realpath((isset($options["templates"])) ? $options["templates"] : realpath("./"));
        $this->auto_reload = (isset($options["auto_reload"])) ? $options["auto_reload"] : true;
        return $this;
    }
    
    public function compile($string){
        if(!isset($this->compiler)){
          $this->compiler = new $this->compiler_class($this);
        }
        return $this->compiler->compile($string);
    }

    public function is_compiled($template_path,$compiled_path){
        if($this->auto_reload == false){
            return true;
        }else{
            if(is_file($compiled_path)){
                if($this->is_fresh($template_path,filemtime($compiled_path))){
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }
    }
    
    public function evaluate($string,Array $context){
        $this->compile($string);
        $class = $this->prefix . sha1($string);
        if(!class_exists($class)){
            $fixed = $this->compiler->make_class(sha1($string),$this->prefix . sha1($string),"","");
            eval('?>' . $fixed);
            $this->evaluates[sha1($string)] = true;
            $this->compiler->clear();
        }

        $template = new $class($this);
        ob_start();
        $template->display($context);
        $result = ob_get_clean();
        return $result;
    }

    public function display(array $context){
        $template_path = $this->dir   . DIRECTORY_SEPARATOR . $this->template;
        $classname = $this->getClassName($template_path);
        $compiled_path = $this->cache . DIRECTORY_SEPARATOR . $classname . ".php";

        if(is_file($compiled_path)){
            $template_path = $this->dir   . DIRECTORY_SEPARATOR . $this->template;
            $classname = $this->getClassName($template_path);
            $compiled_path = $this->cache . DIRECTORY_SEPARATOR . $classname . ".php";

            if(!$this->is_compiled($template_path,$compiled_path)){
                $this->compile(file_get_contents($template_path));
                if($this->extend){
                    $extend_class = $this->prefix . md5($this->dir . DIRECTORY_SEPARATOR . $this->extend);
                }else{
                    $extend_class = "";
                }
                file_put_contents(
                $compiled_path,
                $this->compiler->make_class($this->template,$classname,$extend_class,$this->extend),
                LOCK_EX
                );
                $this->compiler->clear();
            }
            require_once $compiled_path;

            if($_extends = call_user_func(array($classname,"getExtends"))){
                $template_path = $this->dir   . DIRECTORY_SEPARATOR . $_extends;
                $classname = $this->getClassName($template_path);
                $compiled_path = $this->cache . DIRECTORY_SEPARATOR . $classname . ".php";

                if(!$this->is_compiled($template_path,$compiled_path)){
                    $this->compile(file_get_contents($template_path));
                    if($this->extend){
                        $extend_class = $this->prefix . md5($this->dir . DIRECTORY_SEPARATOR . $this->extend);
                    }else{
                        $extend_class = "";
                    }
                file_put_contents($compiled_path,
                    $this->compiler->make_class($name,$classname,$extend_class,$this->extend),
                    LOCK_EX
                );
                $this->compiler->clear();
                }
            }
            $classname = $this->getClassName($this->dir   . DIRECTORY_SEPARATOR . $this->template);
            $compiled_path = $this->cache . DIRECTORY_SEPARATOR . $classname . ".php";
        }else{
            //初回のコンパイル
            $classes = array();
            do{
                if(!empty($this->extend)){
                    $name = $this->extend;
                }else{
                    $name = $this->template;
                }

                $template_path = $this->dir   . DIRECTORY_SEPARATOR . $name;
                $classname = $this->getClassName($template_path);
                $compiled_path = $this->cache . DIRECTORY_SEPARATOR . $classname . ".php";
                unset($this->extend);

                $this->compile(file_get_contents($template_path));
                if(isset($this->extend)){
                    $extend_class = $this->prefix . md5($this->dir . DIRECTORY_SEPARATOR . $this->extend);
                    $extend = $this->extend;
                }else{
                    $extend_class = "";
                    $extend = "";
                }
                $classes[] = $compiled_path;

                file_put_contents($compiled_path,
                    $this->compiler->make_class($name,$classname,$extend_class,$extend),
                    LOCK_EX
                );
                $this->compiler->clear();
            }while(isset($this->extend));

            //$classes = array_reverse($classes);
            $classname = $this->getClassName($this->dir   . DIRECTORY_SEPARATOR . $this->template);
            $compiled_path = $this->cache . DIRECTORY_SEPARATOR . $classname . ".php";
            require_once $compiled_path;
        }
        $template = new $classname($this);
        $template->display($context);
        $this->clear();
    }
}

function def($str,$def){
    if(empty($str)){
        return $def;
    }else{
        return $str;
    }
}

function format($str,$format = "Y-m-d H:i:s"){
  return date($format,strtotime($str));
}
