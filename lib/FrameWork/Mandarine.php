<?php
class Mandarine{
    public $env;
    public $responsehandler;
    public $requesthandler;
    public $dispatcher;
    public $routes;

    public function __construct($yaml){
        $data = @yaml_parse_file($yaml);
        if((bool)$data){
            foreach($data as $key => $value){
                switch($key){
                    case "environment":
                        $this->setEnvironment($value);
                        break;
                    case "routing":
                        $this->setRouting($value);
                        break;
                    case "handler":
                        $this->setRequestHandler($value["request"]);
                        $this->setResponseHandler($value["response"]);
                        break;
                    case "dispatcher":
                        $this->setDispatcher($value);
                        break;
                }
            }
        }
    }
    
    
    public function run(){
        $request    = new $this->requesthandler();
        $response   = new $this->responsehandler();
        $dispatcher = new $this->dispatcher($this->env);

        foreach($this->routes as $conf){
            $dispatcher->add(Mandarine\Router::Load($conf));
        }
        
        try{
            $dispatcher->it($request,$response);
        }catch(\Mandarine\Exception\ActionNotFound $e){
            echo "<!DOCTYPE HTML PUBLIC \"-//IETF//DTD HTML 2.0//EN\">\r\n<html><head>\r\n<title>404 Not Found</title>\r\n</head><body>\r\n<h1>Not Found</h1>\r\n<p>The requested URL %%REQUEST_URI%% was not found on this server.</p>\r\n<hr>\r\n<address>%%SERVER_SOFTWARE%% Server at %%HTTP_HOST%% Port %%SERVER_PORT%%</address>\r\n</body></html>";
            
        }
        $logger = $dispatcher->getLogger();
        $logger->end();
        $logger->show();
    }
    
    public function setRouting($filename){
        $this->routes = yaml_parse_file($filename);
    }
    
    public function setResponseHandler($classname){
        $this->responsehandler = $classname;
    }
    
    public function setRequestHandler($classname){
        $this->requesthandler = $classname;
    }
    
    public function setDispatcher($classname){
        $this->dispatcher = $classname;
    }

    public function setEnvironment($environment = "Development"){
        $this->env = $environment;
    }
}